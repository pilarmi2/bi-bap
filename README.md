# Bakalářská práce / Bachelor thesis, Michal Pilař

- [Momentální verze v PDF / Current version in PDF](/Thesis/ctufit-thesis.pdf)
- [Zdrojový kód pluginu / Plugin source code](/Plugin/)
- [Zdrojový kód práce ve formátu LaTeX / Source code of the thesis in LaTeX format](/Thesis/)
