\chapter{Návrh}
Tato kapitola se věnuje návrhu budoucího pluginu. Nejprve je popsán smysl a~cíl samotného pluginu -- pro jaké uživatele je tento plugin určen a~jaké má být jeho využití. Poté je navržena struktura generovaných budov -- konkrétně typy jednotlivých místností, které bude plugin generovat. Následně je vytvořen návrh samotné funkčnosti -- jakým způsobem se budovy budou generovat a~jaké metody budou při generování použity. Nakonec je navrženo jednoduché uživatelské rozhraní odpovídající požadované funkčnosti.

\section{Využití pluginu}
V~předchozích kapitolách byl popsán proces a možné použití procedurálního generování. Zároveň v~nich bylo vysvětleno, k~jakým účelům se procedurální generování hodí a~k~jakým nikoliv. Výsledný plugin bude spadat do kategorie nástrojů sloužících při \textit{návrhu obsahu}, který byl popsán v~první kapitole (\ref{subsection:contentDesign}). Výsledný plugin má tedy za cíl co nejvíce automatizovat vytváření modelů budov, které budou následně použity při tvorbě videoherních světů, ovšem zároveň počítá se zásahem uživatele do výsledných modelů -- například ruční přesunutí určitých modelů na místo, které je podle uživatele vhodnější.

Nástroje sloužící k~návrhu obsahu mají několik zásadních výhod, které se odráží na jejich vývoji. Na rozdíl od nástrojů, které generují videoherní světy v reálném čase během hraní, nástroj sloužící pro návrh obsahu nemusí generovat modely v~řádu desetin vteřin a~běh samotného generování může být tedy delší -- i~když stále v~rozumných mezích. Zároveň lze plugin implementovat více náhodný za cenu toho, že občas vygeneruje nesmysl, který musí uživatel ručně opravit. Náhodnost ovšem nesmí být ani u~návrhu obsahu přehnaná, aby ruční zásah byl opravdu spíše výjimkou než pravidlem.

\section{Struktura budov}
Budova, kterou bude plugin generovat, se bude skládat z~několika typů místností. Každá místnost bude charakterizována modely, které se do dané místnosti budou generovat, aby byla odlišitelná od ostatních typů. V této části je popsána struktura těchto typů místností a~to, jaké modely se do daných typů místností budou generovat.

\subsection{Místnosti}
Jak již bylo zmíněno, vygenerovaná budova bude rozdělena do několika místností. Tyto místnosti mohou být různého typu. Plugin bude schopen generovat následující místnosti:
\begin{enumerate}
	\item \textit{Společenská místnost}
	\item \textit{Ložnice}
	\item \textit{Kuchyň}
	\item \textit{Chodba}
\end{enumerate}

\subsection{Objekty}
Plugin má za úkol generovat nejen samotnou budovu, ale i~vyplnit její interiér nejrůznějšími objekty. U~některých předmětů existují omezení, ve kterých místnostech se mohou nacházet -- například nedává smysl, aby se na chodbě generovala postel. Naopak by bylo zvláštní, kdyby postel chyběla v ložnici. Plugin bude schopen generovat následující objekty:

\begin{enumerate}
	\item \textit{Postel}
	\item \textit{Stůl se stoličkami}
	\item \textit{Skříň}
	\item \textit{Krb}
	\item \textit{Kuchyňský stůl (obdoba dnešní kuchyňské linky)}
\end{enumerate}

Rozdělení navržených objektů podle místností, ve kterých se mohou nebo musí nacházet, je rozepsáno v~tabulce~\ref{tab:RoomsObjects}.


    
\begin{table}[]
\begin{tabular}{lll}
\hline
\textbf{Typ místnosti} & \textbf{Povinné objekty} & \textbf{Možné objekty} \\ \hline
Ložnice                & Postel       	  		  & Skříň                 \\
Kuchyň                 & Krb, kuchyňský stůl      &                  \\
Chodba                 &                          & Skříň                  \\
Společenská místnost   & Stůl se stoličkami           & Skříň             \\ \hline
\end{tabular}
\caption[Tabulka vztahů mezi jednotlivými objekty a místnostmi]{Tabulka vztahů mezi jednotlivými objekty a~místnostmi}
\label{tab:RoomsObjects}
\end{table}

\section{Funkčnost pluginu}
Plugin bude fungovat ve třech základních krocích. Prvním krokem je vytvoření půdorysu budovy. V~druhém kroku se instance objektu reprezentující jednotlivá patra rozdělí na místnosti. Ve třetím a~posledním kroku se importují jednotlivé modely, které dohromady dávají celou budovu -- zároveň se do jednotlivých místností vygenerují veškeré objekty -- skříně, postele, stoličky, stoly a~další. Tyto tři kroky jsou v~této části podrobněji popsány. Poté jsou popsána jednotlivá pravidla, omezující samotné generování a~zabraňující, aby plugin generoval nesmyslnou změť pokojů a~objektů.

\subsection{Logika generování}
\subsubsection*{Vytvoření půdorysu} \label{chapter:floorPlanAlgo}
Jak již bylo zmíněno, prvním krokem je generování exteriéru budovy. V tomto kroku jsou pomocí pseudonáhodného algoritmu poskládány předem vytvořené modely zdí tak, aby dohromady dávaly vždy celé patro. Uživatel má možnost ovlivnit počet pater. Před tím, než se mohou začít generovat jednotlivá patra se generuje půdorys budoucí budovy. Algoritmus pracuje se zdmi o~délce 5 metrů. Algoritmus na sebe tyto zdi rozumně navazuje, dokud nevytvoří použitelný půdorys budovy. Při vytváření půdorysu ovšem nesmí překročit žádný z~rozměrů délku 20 metrů (budova nesmí být širší nebo hlubší než 20 metrů). Podrobněji tento postup popisuje algoritmus \ref{alg:FloorPlanAlgo}. Algoritmus zároveň zaznamenává okrajové souřadnice rektangulární sítě, do které vygenerovaný půdorys spadá -- tato síť je později použita při rozdělení půdorysu na stejně velké čtvercové plochy, neboli segmenty.

\begin{algorithm}
\caption{Algoritmus vytvoření půdorysu budovy}
\begin{algorithmic}[1]
\State Vytvoř zatím prázdný seznam zdí.
\State Alokuj čtyři souřadnice, které reprezentují rohové souřadnice rektangulární sítě, do které vygenerovaný půdorys spadá. Všechny čtyři souřadnice nastav na hodnoty (0,~0).
\State Vytvoř první zeď, která bude počáteční zdí při vytváření půdorysu budovy. Zeď začíná na souřadnicích (0,~0) a~končí na souřadnicích (5,~0). Veškeré souřadnice, které zeď protne, a~jejichž obě hodnoty jsou násobkem pěti, přidej do seznamu pokrytých souřadnic.
\State Vytvoř novou zeď tak, že vybereš směr, ve kterém novou zeď navážeš na předchozí zeď. Směr určuje, zda budova roste do šířky nebo do hloubky. Možné směry jsou omezeny aktuálním stavem půdorysu, aby nedocházelo k~zacyklení nebo vytváření nesmyslných tvarů.
\If{Po navázání na předchozí zeď by byl překročen limit 20 metrů u~šířky nebo hloubky.}
    \State Vrať se na krok 4.
\ElsIf{Po navázání na předchozí zeď by byl uzavřen půdorys -- konec nové zdi by byl na souřadnici (0, 0)}
	\State Navaž vygenerovanou zeď na zeď předchozí.
	\State Veškeré souřadnice, které zeď protne, a~jejichž obě hodnoty jsou násobkem pěti, přidej do seznamu pokrytých souřadnic -- ten je součástí každé zdi.
	\State Půdorys byl úspěšně uzavřen. Algoritmus končí.
\Else
	\State Navaž vygenerovanou zeď na zeď předchozí.
	\State Veškeré souřadnice, které zeď protne, a~jejichž obě hodnoty jsou násobkem pěti, přidej do seznamu pokrytých souřadnic -- ten je součástí každé zdi.
	\State Pokud vygenerovaná zeď přesahuje ven ze sítě, reprezentované čtyřmi souřadnicemi vytvořenými na začátku algoritmu, aktualizuj tyto souřadnice tak, aby do vzniklé sítě patřila i~nová zeď.
	\State Vrať se na krok 4.
\EndIf
\end{algorithmic}
\label{alg:FloorPlanAlgo}
\end{algorithm}

Po vytvoření půdorysu se ještě testuje, zda vygenerovaný půdorys splňuje požadavky dané budovy -- pokud ne, generujeme půdorys znovu. Tyto požadavky jsou podrobněji popsány v sekci \textit{Návrh pravidel}. Pokud půdorys požadavky budovy splňuje, tento krok končí.

\subsubsection*{Rozdělení pater na místnosti} \label{chapter:createRoomsAlgo}
V~druhém kroku, který rozděluje vygenerovaná patra na jednotlivé místnosti, je potřeba provést několik dílčích činností. Kromě samotného rozdělení patra na místnosti a~vytvoření zdí, oddělujících tyto místnosti, je potřeba vytvořit v~jednotlivých patrech podlahy a~zároveň schodiště, vedoucí z~jednoho patra do druhého (pokud má budova více pater). Umístění schodiště samozřejmě ovlivňuje jak rozložení místností v~patře, kde schodiště začíná, tak i~v~patře, kde schodiště končí. Z~toho důvodu je potřeba brát na schodiště ohled při rozdělování místností -- nechceme například, aby nám schodiště vedlo přímo do ložnice. Místnosti, ve kterých se může nacházet schodiště jsou chodba a~společenská místnost. V~ložnici a~kuchyni schodiště nesmí ani začínat, ani končit.

Algoritmus pro rozdělení pater na místnosti nejprve rozdělí půdorys na jednotlivé segmenty o~šířce 5~metrů a~hloubce 5~metrů. K~tomuto rozdělení slouží rektangulární síť segmentů, kterou nám reprezentují souřadnice alokované při generování půdorysu. Postup při rozdělování půdorysu na jednotlivé segmenty je popsán algoritmem \ref{alg:FloorPlanSegmentsAlgo}.

\begin{algorithm}
\caption{Algoritmus rozdělení půdorysu na jednotlivé segmenty}
\begin{algorithmic}[1]
\State Vytvoř zatím prázdný seznam segmentů patřících k~půdorysu.
\State Pro každý řádek rektangulární sítě projdi jednotlivé segmenty následovně.
\State Vyber první segment v~řádku a~nastav jeho stav na neznámý.
\If{Stav zvoleného segmentu je neznámý.}
	\If{Levá hrana vybraného segmentu je na našem půdorysu překryta zdí.}
		\State Přidej segment do seznamu segmentů patřících k~půdorysu.	
		\If{Pravá hrana vybraného segmentu je na našem půdorysu překryta zdí.}
			\State Stav následujícího segmentu bude udávat, že nepatří k~půdorysu.
			\State Přejdi na další segment a~pokračuj krokem 4.
		\Else
			\State Stav následujícího segmentu bude udávat, že patří k~půdorysu.
			\State Přejdi na další segment a~pokračuj krokem 4.
		\EndIf
	\Else
		\If{Pravá hrana vybraného segmentu je na našem půdorysu překryta zdí.}
			\State Stav následujícího segmentu bude udávat, že patří k~půdorysu.
			\State Přejdi na další segment a~pokračuj krokem 4.
		\Else
			\State Stav následujícího segmentu bude udávat, že nepatří k~půdorysu.
			\State Přejdi na další segment a~pokračuj krokem 4.
		\EndIf
	\EndIf
\ElsIf {Stav zvoleného segmentu udává že je součástí půdorysu.}
	\State Přidej segment do seznamu segmentů patřících k~půdorysu.
	\If{Pravá hrana vybraného segmentu je na našem půdorysu překryta zdí.}
		\State Stav následujícího segmentu bude udávat, že nepatří k~půdorysu.
	\Else
		\State Stav následujícího segmentu bude udávat, že patří k~půdorysu.
		\State Přejdi na další segment a~pokračuj krokem 4.
	\EndIf
\ElsIf {Stav zvoleného segmentu udává že není součástí půdorysu.}
	\If{Pravá hrana vybraného segmentu je na našem půdorysu překryta zdí.}
		\State Stav následujícího segmentu bude udávat, že patří k~půdorysu.
		\State Přejdi na další segment a~pokračuj krokem 4.
	\Else
		\State Stav následujícího segmentu bude udávat, že nepatří k~půdorysu.
		\State Přejdi na další segment a~pokračuj krokem 4.
	\EndIf
\EndIf
\end{algorithmic}
\label{alg:FloorPlanSegmentsAlgo}
\end{algorithm}

Po rozdělení půdorysu na segmenty následuje samotné rozdělení na místnosti. Algoritmus po každém rozdělení zadané plochy na polovinu kontroluje, zda je aktuální plocha vhodná pro danou místnost nebo zda je potřeba provést další dělení. Algoritmus samotného rozdělení půdorysu na jednotlivé místnosti je popsán algoritmem \ref{alg:CreatingRooms}. Tento algoritmus je založen na \textit{metodě dělení prostoru} s~využitím BSP stromu. Tato metoda byla více popsána v první kapitole (\ref{chapter:bsp}).

\begin{algorithm}
\caption{Algoritmus rozdělení půdorysu na místnosti}
\begin{algorithmic}[1]
\State Vytvoř kořen BSP stromu -- vlož do něj všechny zdi a~všechny segmenty, které patří k~půdorysu. Tento kořen vyber jako první zpracovávaný vrchol.
\If {Právě zpracovávaná oblast již nepotřebuje dále dělit.}
	\State Algoritmus končí.
\EndIf
\State Pseudonáhodně vyber jeden ze souřadnicových bodů, který patří k~jedné ze zdí právě zpracovávaného vrcholu BSP stromu.
\State Pseudonáhodně vyber druhý bod, který má stejnou \textit{x}~nebo \textit{y}~souřadnici, jako první zvolený bod, a~patří k~jedné ze zdí právě zpracovávanému vrcholu.
\If{Zeď protínající oba body patří do půdorysu a~zároveň by vzniklá zeď neprotínala již existující zeď.}
	\State Vytvoř zeď.
	\State Do jednoho potomka právě zpracovávaného vrcholu stromu přidej veškeré zdi a~segmenty na jedné straně od nově vytvořené zdi, do druhého potomka přidej veškeré zdi a~segmenty na druhé straně od nově vytvořené zdi.
	\State Pro oba nově vytvořené potomky opakuj algoritmus od kroku 2.
\Else
	\State Vrať se na krok 2~a~opakuj algoritmus.
\EndIf
\end{algorithmic}
\label{alg:CreatingRooms}
\end{algorithm}

V rámci algoritmu rozdělení půdorysu na místnosti (\ref{alg:CreatingRooms}) je potřeba umět provést dva kroky. Nejprve potřebujeme zjistit, zda nám nově vzniklá zeď vůbec spadá do půdorysu -- tento proces je popsán algoritmem \ref{alg:IsPartOfFloorPlan}. Poté ověřujeme, zda nově vzniklá zeď neprotíná nějakou již existující zeď. To je popsáno algoritmem \ref{alg:ThereIsWallAlready}.

\begin{algorithm}
\caption{Algoritmus ověření, zda nová zeď spadá do půdorysu budovy.}
\begin{algorithmic}[1]
\State Vytvoř hranu (budoucí zeď) mezi zadaným bodem \textit{a}~a~bodem \textit{b}. Hrana je reprezentována všemi protnutými souřadnicemi, jejichž \textit{x}~a~\textit{y}~souřadnice jsou násobkem pěti.
\State Pro každý bod náležící budoucí zdi ověř, že patří k~nějakému segmentu půdorysu.
\If{Všechny dvojice sousedních bodů spadají do půdorysu budovy}
	\State Nová zeď spadá do půdorysu budovy. Algoritmus končí.
\Else
	\State Nová zeď nespadá do půdorysu budovy. Algoritmus končí.
\EndIf
\end{algorithmic}
\label{alg:IsPartOfFloorPlan}
\end{algorithm}

\begin{algorithm}
\caption{Algoritmus ověření, zda nová zeď neprotíná již existující zeď.}
\begin{algorithmic}[1]
\State Vytvoř hranu (budoucí zeď) mezi zadaným bodem \textit{a}~a~bodem \textit{b}. Hrana je reprezentována všemi protnutými souřadnicemi, jejichž \textit{x}~a~\textit{y}~souřadnice jsou násobkem pěti.
\State Pro každý bod na budoucí zdi (vyjma zadaných bodů \textit{a}~a~\textit{b}) zkoumej, zda již nepatří k~existující zdi.
\If{Žádný ze zkoumaných bodů není součástí existující zdi.}
	\State Nová zeď neprotíná žádnou existující zeď. Algoritmus končí.
\Else
	\State Nová zeď protíná existující zeď. Algoritmus končí.
\EndIf
\end{algorithmic}
\label{alg:ThereIsWallAlready}
\end{algorithm}

Listy vzniklého BSP stromu nám reprezentují jednotlivé místnosti. Po vytvoření tohoto stromu je tedy závěrem každému listu přiřazen typ místnosti.

\subsubsection*{Generování interiéru}
Závěrečným krokem je generování interiéru. Předměty jsou pseudonáhodně rozmístěny do místnosti dle daných pravidel -- například zda se daný předmět musí nacházet u~zdi nebo naopak uprostřed segmentu. Metoda, která rozmisťuje předměty do místností dostane zadaný počet objektů. Následně pro každý segment dané místnosti rozhoduje, zda se v~něm bude nebo nebude nacházet daný předmět. Pokud metoda rozhodne, že se v~daném segmentu předmět nacházet bude, zvolí jeho umístění v tomto segmentu dle zadaných pravidel.

\subsection{Návrh pravidel}
To, jakým způsobem se do budov budou generovat místnosti, a~do těchto místností jednotlivé předměty, bude omezeno sadou pravidel. Tato pravidla budou určovat například kolikrát se může daný typ místnosti vyskytovat v budově. Jako nejzákladnější pravidla můžeme chápat vztahy mezi jednotlivými typy místností a~mezi objekty, které byly dříve popsány. Následující pravidla tato omezení ještě rozšiřují, aby vygenerované budovy měly rozumnou strukturu. Jednotlivá pravidla znějí následovně:

\subsubsection*{Pravidla omezující generování}
\begin{enumerate}
	\item V~budově se nesmí nacházet více než jedna místnost typu \textit{kuchyň} na patře.
	\item Postel se musí vždy nacházet u~zdi.
	\item Skříň se musí vždy nacházet u~zdi, otočená dveřmi do prostoru.
	\item Krb se musí vždy nacházet u~zdi.
	\item Stůl se stoličkami ve společenské místnosti se vždy nachází uprostřed segmentu.
	\item Stůl v~kuchyni se vždy nachází uprostřed segmentu.
	\item Schodiště může začínat a~ústit pouze v~místnostech typu \textit{chodba} a~\textit{společenská místnost}.
\end{enumerate}


\section{Návrh uživatelského rozhraní pluginu}
Vzhledem k~tomu, že výsledný plugin je zamýšlen tak, aby mohl být v~budoucnu rozšířen v~generátor celých měst, není potřeba vytvářet komplikované uživatelské rozhraní. Z~toho důvodu bude mít uživatel při používání pluginu pouze tři možnosti, jak vzhled výsledné budovy ovlivnit. Uživatel může zvolit typ budovy, počet pater a~velikost budovy. Ač vytvořený plugin bude generovat pouze jediný typ budovy, kterým bude \textit{obytný dům}, tato položka slouží k~budoucímu rozšíření o~další typy, jakými mohou být například hostince nebo obchody. Uživatel bude dále schopen nastavit, zda má mít budova jedno až čtyři patra a~zda má být její rozloha malá, střední nebo velká. Velikost rozlohy chápeme jako počet pokrytých segmentů. Návrh uživatelského rozhraní je vidět na obrázku  \ref{fig:userInterfaceDesign}.

\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth, keepaspectratio]{design/userInterface}
	\caption[Návrh uživatelského rozhraní]{Návrh uživatelského rozhraní}
	\label{fig:userInterfaceDesign}
\end{figure}
