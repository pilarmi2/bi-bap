%---------------------------------------------------------------
\chapter{Rešerše}
%---------------------------------------------------------------
Tato kapitola se věnuje vytváření videoherního prostředí. Je v~ní vysvětlen produkční proces při vytváření takového prostředí, hlavně jednotlivých objektů. Následně je popsána metoda procedurálního generování, jakým způsobem ji použít při tvorbě herního světa, jaké jsou její výhody a~nevýhody.

\section{Tvorba prostředí počítačové hry}
V~této části je definován pojem \textit{prostředí počítačové hry}. Následně je vysvětlen rozdíl mezi prostředím počítačové hry a~level designem. Závěrem se tato část věnuje produkčnímu procesu při vytváření takového prostředí.

Při vývoji počítačových her se můžeme setkat s~řadou pojmů. Pro účely této práce je důležité si definovat pojem \textit{prostředí počítačové hry} a~vysvětlit si rozdíl mezi tímto pojmem a~pojmem \textit{level design}. V~knize \textit{Creating Game Environments in Blender 3D}~\cite{guevarra2020creating} je prostředí počítačové hry definováno následovně:

\begin{definition}[Prostředí počítačové hry]
Prostředí počítačové hry (Game Environment) je dimenzí spojující pravidla, cíle, předměty a~teoretické aspekty do jednoho celku, přinášející interaktivní tok aktivit.
\end{definition}

Pojem \textit{game environment} je často zaměňován s~pojmem \textit{level design}. Tyto dva pojmy se ovšem zásadně liší. Zatímco \textit{game environment} nám udává estetické a~architektonické působení dané hry, \textit{level design} určuje například to, jaké výzvy musí hráč během svého hraní překonat, nebo jak je celý svět poskládán do jednoho smysluplného celku. Podrobněji popsal tento rozdíl \textit{Josh Bycer} ve svém článku \textit{Level Design vs. Environment Design}~\cite{bycer_2020} nebo \textit{Luke Ahearn} v~knize \textit{3D game environments}~\cite{ahearn20173d}. Zjednodušeně by se dalo prohlásit, že \textit{game environment} je určitou podmnožinou \textit{level designu}.

Nedílnou součástí každého prostředí počítačové hry je videoherní obsah (Game Assets). Ten je v~již zmíněné knize \textit{Creating Game Environments in Blender 3D}~\cite{guevarra2020creating} popsán následující definicí: 

\begin{definition}[Videoherní obsah (Game Assets)]
Videoherním obsahem se rozumí cokoliv, co je součástí hry -- objekty, scény, materiály, textury, osvětlení a~další.
\end{definition}

V následujících částech této kapitoly jsou popsány praktické části procesu tvorby prostředí počítačové hry -- nevěnujeme se tedy level designu obecně. Je zde popsán proces tvorby takového videoherního obsahu, který určuje vizuální působení prostředí počítačové hry.

\section{Vytváření modelů}
Nedílnou součástí vytváření jakéhokoliv virtuálního prostředí je tvorba jednotlivých 3D modelů, které reprezentují jednotlivé objekty ve hře. To může být komplikovanější proces, než by se mohlo na první pohled zdát.

Jak popisuje \textit{Renee Dunlop} ve své knize \textit{Production Pipeline Fundamentals for Film and Games}~\cite{dunlop2014production}, to, jakým způsobem jsou modely vytvořeny, se odvíjí od toho, k jakému účelu modely slouží. Je totiž velký rozdíl, zda se vytváří modely, které mají sloužit při tvorbě vizuálních efektů filmu, nebo zda bude model použit  v~počítačové hře. Narozdíl od počítačové hry, u~filmu nemá konzument možnost ovlivnit, jak se bude jeho obsah vyvíjet. Akce, úhly kamery a~tempo, ve kterém se děj odvíjí jsou celé rozhodnuty již při produkci a~výsledek je lineární obrazovou a~zvukovou sekvencí.

Oproti tomu, ač u~počítačových her produkční tým zpravidla vymýšlí příběhovou linii, a~rozhoduje vizuální a~zvukový styl celé hry, je to nakonec samotný konzument, který rozhodne, co se bude s~vytvořeným obsahem dít. Hráč rozhoduje, z~jakého úhlu se bude akce zobrazovat, v~jakém pořadí se bude zobrazovat a~často i~co se bude zobrazovat.

To, jakým způsobem model vytváříme, se přímo odvíjí od toho, zda je potřeba ho zobrazovat v reálném čase (jako u zmíněných počítačových her), nebo zda si můžeme dovolit vykreslovat jeden snímek v řádu minut až hodin (jak tomu je při tvorbě vizuálních efektů). Ač při vytváření jednotlivých komponent často tvůrci filmů a~počítačových her používají stejné nástroje, liší se způsob, jakým jsou tyto nástroje použity. Pokud totiž chceme vykreslit jednotlivé snímky v~reálném čase, potřebujeme, aby zobrazení modelu nebylo příliš výpočetně náročné. Z~toho důvodu si například nemůžeme dovolit mít veškeré detaily přímo v~geometrii modelu, ale musíme si pomoci jiným způsobem, což zde jsou zpravidla textury.

Samotné modelování je pravděpodobně nejkomplexnější proces při tvorbě kompletních modelů. Jednotlivé modely se skládají ze tří základních prvků -- z~bodů, hran a~ploch. Následující definice jsou převzaty z knihy \textit{Beginning Blender}~\cite{flavell2011beginning}:

\begin{definition}[Vrcholy]
Model je sestaven z~řady bodů, které nazýváme vrcholové body (vertex points / vertices). Tyto vrcholy jsou samy o~sobě nic neříkající a~potřebují něco víc k~tomu, aby definovaly objekt.
\end{definition}

\begin{definition}[Hrany]
Jednotlivé vrcholy jsou propojeny přímkami, které nazýváme jednoduše hrany (edges). Pomocí hran je již možné zobrazit objekt jako formu drátěného objektu (wireframe).
\end{definition}

\begin{definition}[Plochy]
Plochy (faces), též nazývané polygony, nám vznikají, pokud je prostor mezi několika (minimálně třemi -- trojúhelník) spojenými hranami vyplněný, čímž tvoří pevný povrch. Z~ploch jsme schopni vytvořit solidní objekt. Jednotlivé plochy mohou být různě zbarvené za účelem vytvoření realistických textur objektu.
\end{definition}

\textit{R. Dunnlop} ve své knize~\cite{dunlop2014production} popisuje, že počet ploch při produkci filmu může dosahovat až desítek milionů. U~her mají modely zpravidla nižší rozlišení a~detaily jsou přidány později, například pomocí promyšleného texturování a~stínování. Zvyšující se výkon herního hardwaru ovšem umožňuje i~růst počtu ploch u~počítačových her.

Problém optimalizace modelů je popsán v knize \textit{Creating Game Environments in Blender 3D}~\cite{guevarra2020creating}. Polygony mohou mít teoreticky libovolný počet hran, ovšem při vykreslování jsou rozděleny na jednotlivé trojúhelníky. Obecně pak můžeme prohlásit, že čím větší je počet trojúhelníků, ze kterých se objekt skládá, tím je objekt detailnější. Se zvyšujícím se počtem trojúhelníků ovšem roste náročnost vykreslení daného objektu počítačem. K~optimalizaci celé scény je tudíž důležité držet počet trojúhelníků, ze kterých se objekt skládá, co nejnižší.

\subsection*{Modelování vs Sculpting}
Samotné vytváření modelů můžeme rozdělit do dvou podkategorií, respektive do dvou technik, které je možné k~vytváření modelů použít -- \textit{modelování} a~\textit{sculpting}\footnote{Sculpting by se dal přeložit jako \textit{sochání}, ovšem i~mezi česky mluvícími 3D grafiky se z~pojmu \textit{sculpting} stal známý termín, který je lepší ponechat bez překladu, aby nemátl čtenáře a~ten byl schopen si bez problému na toto téma dohledat další informace.}. \textit{James Taylor} ve svém článku~\cite{taylor_2016} popisuje rozdíly mezi těmito dvěma technikami a~zároveň zde vysvětluje, pro jaký typ modelů se jaká technika více hodí. Ač se na první pohled může zdát, že pomocí obou technik dojdeme snadno ke stejným výsledkům -- obě techniky slouží k~tomu, abychom vytvořili geometrii pro naše postavy, prostředí a~další videoherní obsah -- rozdíly jsou zde zásadní.

\subsubsection*{Modelování}
\begin{figure}
	\centering
	\includegraphics[width=1.0\textwidth, keepaspectratio]{research/ModelingVSSculptingModeling}
	\caption[Model vytvořen technikou modelování]{Model vytvořen technikou modelování z~článku \textit{Modeling vs Sculpting}~\cite{taylor_2016}.}
	\label{fig:ModelingVSSculptingModeling}
\end{figure}

Modelováním se rozumí proces, kdy manipulujeme s~jednotlivými polygonálními tvary. S~objekty se pracuje na úrovni jednotlivých komponent -- tedy vrcholů, hran a~ploch -- s~cílem vytvořit komplexnější objekt. Při správné čtyřúhelníkové topologii modelu můžeme velmi efektivně manipulovat s~velkými oblastmi našeho modelu pomocí rychlého výběru kružnic a~smyček skládajících se z~na sebe navazujících hran. Zároveň můžeme dosáhnout vysoké přesnosti výběrem a~manipulací individuálních komponent našeho modelu. To nám umožní provádět velmi specifické změny na modelu -- ovšem za cenu zpomalení pracovního postupu.

Modelování se hodí při vytváření předmětů s~tvrdým povrchem, což jsou zpravidla strojově či člověkem vytvořené předměty, které se skládají například z~rovnoběžných linek nebo pravých úhlů. Modelování je také důležité z~technické stránky -- schopnost ovlivnit topologii daného modelu je klíčová, pokud vytváříme model určený k~animování. Vyhnout se modelování je naopak rozumné při vytváření organických objektů. Příklad modelu, při jehož tvorbě byla použita technika modelování je na obrázku \ref{fig:ModelingVSSculptingModeling}.

\subsubsection*{Sculpting}
\begin{figure}
	\centering
	\includegraphics[width=1.0\textwidth, keepaspectratio]{research/ModelingVSSculptingSculpting}
	\caption[Model vytvořen technikou sculpting]{Model vytvořen technikou sculpting z~tutoriálu \textit{Concept Sculpting for Film and Games}~\cite{zbrush_flipped_normals} od autorů webové stránky \textit{www.flippednormals.com}.}
	\label{fig:ModelingVSSculptingSculpting}
\end{figure}


Sculpting se oproti modelování spoléhá na manipulaci s~objektem pomocí štětce. Při sculptingu nemanipulujeme s~konkrétními komponenty modelu, ale místo toho model deformujeme pomocí nejrůznějších štětců. To činí sculpting extrémně rychlou a intuitivní metodou -- nevybíráme zde totiž jednotlivé vrcholy, se kterými budeme manipulovat, ale manipulujeme s~větším počtem vrcholů najednou pomocí štětce. Sculpting ovšem oproti modelování postrádá přesnost -- je zde velmi obtížné, až nemožné, manipulovat s~jednotlivými komponenty modelu. Jednotlivé štětce zároveň většinou vytvářejí hladký povrch, kvůli čemuž je náročné vytvářet ostré hrany.

Sculpting je nejlepším nástrojem pro vytváření organických modelů, jako jsou postavy, příšery, rostliny nebo oblečení. Pomocí sculptingu lze také velmi rychle vytvořit vysoce detailní modely. Jak už z textu vyplývá, sculpting se nehodí na vytváření objektů s~tvrdým povrchem. Ač u~většiny sculpting nástrojů najdete možnost, jak takové modely vytvořit, je jednodušší pro modely vyžadující přesnost použít modelování. Objekty vytvořené sculptingem mají také téměř vždy moc vysoké rozlišení na to, aby mohly být použity k~animování -- u~takových modelů je potřeba provést proces retopologie. Retopologií se rozumí vymodelování stejného modelu, jaký byl vytvořen pomocí sculptingu, ovšem s~mnohem nižším detailem a~se správnou topologií. Detailní model vytvořený sculptingem je zde následně použit k vytvoření textur, které dodají detail tam, kde ho model s~nízkým rozlišením postrádá. Příklad modelu, při jehož tvorbě byla použita technika sculpting je na obrázku \ref{fig:ModelingVSSculptingSculpting}.


\section{Materiály a textury}
Materiály přidávají objektům nejrůznější optické vlastnosti, například s~cílem, aby se jejich vzhled co nejvíce přiblížil vzhledu objektu v~reálném světě. Jsou nejčastěji reprezentovány sadou textur. Jejich cílem mimo jiné je, aby přidaly detaily tam, kde není možné je reprezentovat samotnou geometrií. Přiřazování textur (respektive materiálů) objektu nazýváme \textit{mapování textur}. Definice tohoto pojmu je převzata z~knihy \textit{Beginning PBR Texturing}~\cite{kumar2020beginning} a~zní následovně.

\begin{definition}[Mapování textur]
Mapování textur (Texture mapping) je proces přiřazování barev jednotlivým pixelům na 3D modelu s~cílem simulovat čtyři atributy: barvu (color), odráženou barvu (specular color), nerovnost povrchu (surface perturbation) a~průhlednost (transparency).
\end{definition}

\subsection{UV unwrapping}
Vzhledem k~tomu, že materiály pro 3D modely jsou zpravidla reprezentovány pomocí 2D obrázků, neboli textur, je potřeba u~každého modelu, kterému chceme přiřadit materiál, projít procesem, který se nazývá \textit{UV unwrapping}. Tento proces definujeme následovně:

\begin{definition}[UV unwrapping]
\textit{UV unwrapping} je metoda, pomocí které vytváříme takzvané UV mapy, neboli 2D reprezentaci libovolného 3D modelu.
\end{definition}

Příklad UV mapy se nachází na obrázku \ref{fig:UVUnwrapping}. UV unwrapping je potřeba provést u~každého modelu, který chceme později texturovat. Dnes již ovšem existují nástroje, které nám tento proces značně usnadní nebo ho za nás provedou celý. Automaticky provedený UV unwrapping ovšem nemusí vyhovovat našim potřebám a~zásahu do finální podoby UV mapy se tedy často nevyhneme.

\begin{figure}
\begin{subfigure}{.50\textwidth}
  \centering
  \includegraphics[width=0.95\linewidth]{research/UVUnwrappingModel}
  \caption{}
  \label{fig:UVUnwrappingModel}
\end{subfigure}
\begin{subfigure}{.50\textwidth}
  \centering
  \includegraphics[width=0.95\linewidth]{research/UVUnwrappingUVMap}
  \caption{}
  \label{fig:UVUnwrappingUVMap}
\end{subfigure}
\caption[UV Unwrapping]{Na obrázku \ref{fig:UVUnwrappingModel} je zobrazen 3D model. Na obrázku \ref{fig:UVUnwrappingUVMap} je zobrazena UV mapa (2D reprezentace) tohoto modelu.}
\label{fig:UVUnwrapping}
\end{figure}

\subsection{Typy textur}
V knize \textit{Beginning PBR Texturing}~\cite{kumar2020beginning} jsou popsány jednotlivé typy textur, jejichž kombinací vzniká požadovaný vzhled daného modelu. To, jaké konkrétní typy textur se pro daný model použijí, závisí na použitém herním enginu a~na pracovním procesu studia vyvíjejícího hru. Typy textur mohou být následující.

\begin{figure}
	\centering
	\includegraphics[width=1.0\textwidth, keepaspectratio]{research/PBRWorkflow}
	\caption[Ukázka metallic-roughness a~specullar-glossines přístupů]{Ukázka \textit{metallic-roughness} a~\textit{specullar-glossines} přístupů z~tutoriálu publikovaného společností \textit{Adobe} na téma PBR texturování~\cite{pbr_guide_2}.}
	\label{fig:PBRWorkflow}
\end{figure}

\begin{enumerate}
	\item \textit{Diffuse map}. Difuzní mapa reprezentuje celkovou barvu modelu. Jedná se o~nejběžnější typ textury. Tato textura zároveň nese informaci o~světlech a~stínech.
	\item \textit{Albedo map}. Albedo mapa je podobná difuzní mapě, pouze s~tím rozdílem, že nenese žádnou informaci o~světle a~stínech.
	\item \textit{Transparency/alpha map}. Většinou se jedná o~mapu obsahující pouze stupně šedi. Tradičně bílá barva na této mapě znamená, že objekt je zcela viditelný a~černá barva naopak znamená, že objekt je zcela průhledný. Odstíny šedi mezi černou a~bílou barvou nesou informaci, jak moc je objekt průhledný.
	\item \textit{Specular map}. Tato mapa určuje odlesk povrchu v~daných místech.
	\item \textit{Bump map}. Bump mapa je určena k~tomu, aby vytvořila iluzi nerovnosti povrchu. Může se použít k~vytvoření škrábanců, odřenin a~jiných drobných povrchových detailů. Iluze hloubky závisí na úhlu kamery mířící na objekt. Čím více se hodnota mapy blíží k~bílé, tím více detaily vystupují z~povrchu a~naopak. Výsledný efekt nezávisí na jednotlivých vrcholech modelu.
	\item \textit{Normal map}. Tento typ mapy se používá k~zobrazení detailů s~vysokým rozlišením na modelu s~nízkým rozlišením. Textura mění směr normály v~místě daného pixelu pomocí chorochromatické mapy.
	\item \textit{Displacement map}. Tato mapa na rozdíl od bump mapy závisí na vrcholech modelu. Displacement mapa generuje fyzickou hloubku modelu podle toho, jaký odstín šedé se nachází v daném místě mapy.
	\item \textit{Ambient occulsion map}. Tato mapa se používá pro generování stínů a~tmavých míst na základě blízkosti objektů.   
\end{enumerate}

Existují i~další typy map, které mohou být užitečné pro dosažení nejlepších možných výsledků, jako jsou metallic mapy, parallax mapy, glossiness mapy, roughness mapy a~další. Jak již bylo zmíněno dříve, to, jaké mapy budou při vývoji počítačové hry potřeba závisí na tom, jaký engine se při vývoji používá. Pokud je používaný engine založen na takzvaném \textit{mettalic-roughness} přístupu, musí být použity albedo, metallic, roughness, normal a~ambient occulsion mapy. Tento přístup používá například engine \textit{Unreal Engine 4}. Pokud ovšem daný engine používá \textit{specular-glossiness} přístup, musí být použity diffuse, specular, glossiness, normal a~ambient occulsion mapy. Na tomto přístupu je založen například herní engine \textit{Unity}. Příklad použití těchto dvou přístupů je zobrazen na obrázku \ref{fig:PBRWorkflow}.

\section{Úvod do procedurálního generování}
Jak je již zmíněno v~úvodu, procedurální generování je metoda, díky které lze předat část tvůrčího procesu do rukou algoritmu. Původ a~dodnes největší využití má procedurální generování ve vývoji počítačových her.

Definice se v~různých pramenech drobně liší, ovšem myšlenka zůstává stejná. \textit{Gillian Smith}~\cite{smith2015analog} například definoval procedurální generování obsahu následovně (jedná se o~velmi obecnou definici, která pokrývá veškeré formy procedurálního generování):

\begin{definition}[Procedurální generování obsahu -- PCG]
Procedurálním generováním obsahu se rozumí použití formálního algoritmu k~vygenerování obsahu, který by byl jinak vytvořen lidskou rukou.
\end{definition}

\textit{Tanya X. Short} a~\textit{Tarn Adams} ve své knize~\cite{short2017procedural} popisují, že ručně vyráběný obsah může paradoxně působit více uměle, než obsah vytvořený počítačem. Lidští designéři mají často potřebu to přehánět s~množstvím designových prvků a~vést hráče až moc lineární cestou -- to může způsobit, že se hráč cítí méně jako dobrodruh a~více jako malé dítě vedené za ruku. V tomto smyslu může být lhostejnost algoritmu vůči hráči velikou výhodou. Stejně jako skutečný svět, procedurálně generovaný obsah se nezajímá o~to, zda na konci naší cesty čeká něco zajímavého. Používání procedurálního generování ovšem neznamená, že do vytvořeného díla nezasáhne vůbec lidská ruka. Tento zásah ovšem udává pouze širší směr, kterým se bude výsledný obsah ubírat. V knize~\cite{short2017procedural} jsou následně popsány tři příklady, ve kterých lze PCG použít -- \textit{návrh obsahu pomocí PCG}, \textit{modální PCG} a~\textit{segmentované PCG}.

\subsection{Návrh obsahu pomocí PCG} \label{subsection:contentDesign}
PCG je ve vývoji her často použité k~vygenerování velkého množství obsahu, který je následně ručně upraven. Typický případ tohoto přístupu jsou hry s~velkým otevřeným světem, jako je například \textit{Skyrim}, jehož součástí je právě velký svět, který může hráč prozkoumat. Dalším příkladem mohou být logické hry -- v~tomto případě generátor vytvoří stovky logických úloh odpovídajících dané hře a~člověk ručně vybere ty nejzajímavější. V těchto případech přichází rozhodnutí použít PCG brzy, ovšem jeho implementace není klíčová v~počátcích projektu. U~generátoru krajiny pro RPG hru (hru na hrdiny), jako je již zmíněný Skyrim, nám stačí, když je schopen generovat hrubý návrh -- všechny důležité detaily, se kterými hráč interaguje jsou následně doplněny ručně. Díky tomu je možné lépe předpovědět celkovou časovou náročnost projektu. V~tomto případě použití PCG začíná a~končí v~raných fázích projektu, následně se pouze upravuje vygenerovaný obsah.


\subsection{Modální PCG}
Některé hry spoléhají minimálně na PCG, ale mají speciální módy hraní, které využívají procedurálně vygenerovaný obsah. Jedná se o~vedlejší část hry, která je nějakým způsobem napojena na část hlavní -- hráč má například možnost vedle plnění hlavní příběhové linie prozkoumávat procedurálně generované jeskyně, ovšem může se tomuto obsahu zcela vyhnout. Vzhledem k~tomu, že se jedná o~oddělené módy, mohou být vytvářeny až v~pozdější fázi projektu nebo dokonce vydány jako rozšíření dávno po vydání původní hry.

Procedurálně generovaný mód, který nějakým způsobem napodobuje původní hru, se může ukázat jako méně nákladný, co se zdrojů týče, neboť návrháři již mají jasnou vizi, jak má výsledný produkt vypadat a~jak mají fungovat jednotlivé herní mechaniky.

Na druhou stranu, tento způsob svádí k~vytváření nepříliš zábavného obsahu, který je více náhodný, než procedurální. Většina her, do kterých byl takový mód implementován se často spoléhá například pouze na vlny náhodně generovaných nepřátel, namísto vytváření zábavného a pohlcujícího procedurálně generovaného herního zážitku.

\subsection{Segmentovaný PCG}
Určité segmenty ve hře mohou být odděleny od ostatního videoherního návrhu s~cílem použít PCG. Lineární, ručně navržená hra může mít například procedurálně generovanou hudbu. Díky tomu, že se jedná o~segment oddělený od zbytku hry, je možné při výskytu nějakého problému přejít k~ručně vytvořenému obsahu, aniž by se výrazně zvýšila časová náročnost.


\section{Výhody a nevýhody PCG}
Většina zásadních výhod a~nevýhod již vyplývá z~předchozích částí textu. V~této části jsou shrnuty zásadní výhody a~nevýhody použití PCG, jež jsou podrobněji popsány v~již zmíněné knize \textit{Procedural Generation in Game Design}~\cite{short2017procedural}.

\subsection{Nevýhody}
\subsubsection*{Zajištění kvality}
Jedním z~největších problémů při použití PCG ve hrách, konkrétně v~rozsáhlých AAA titulech je zajištění kvality, zkráceně QA. Hra, která hojně využívá PCG nemusí být schopna garantovat správnou funkčnost ve všech případech. Je prakticky nemožné otestovat každou možnou iteraci procedurálního generování. Generátor může chybovat v~pouhých 0,1\% případů, na které se nepřijde, dokud není hra vydána pro širokou veřejnost.

\subsubsection*{Časová omezení}
Ač je PCG často chápáno jako nástroj, který vývojářům ušetří čas, opak může být pravdou. Vývoj generátoru může být náročnější, než se původně očekávalo a~tím výrazně zvýšit časovou náročnost celého projektu. Práce na generátoru může být zároveň potřebná i~po jeho dokončení, kvůli nutnosti častých úprav, které jsou potřeba v~závislosti na dalším vývoji. Ruční vytváření obsahu lze mnohem snadněji časově odhadnout.

\subsubsection*{Přehnaná náhodnost}
Pokud projekt nemá dostatečné zdroje nebo dostatečný čas k~vytvoření zajímavého procedurálně generovaného obsahu, může obsah ve hře skončit jako až moc náhodný, čímž se hra stává nudnou a~repetitivní. V takovém případě je lepší se použití PCG vyhnout.

\pagebreak
\subsection{Výhody}
\subsubsection*{Šetření času}
Oproti časovým omezením uvedeným v~předchozí části může PCG při správném použití ve skutečnosti čas šetřit. To se projeví hlavně v~případě, kdy chceme vygenerovat takové množství obsahu, že vytvořit ho ručně by mohlo být prakticky nemožné. Za příklad můžeme dát opět RPG hry s~velkým otevřeným světem, kde ač je vygenerovaný výsledek ručně upravován, šetří zde PCG spoustu času a~zdrojů.

\subsubsection*{Rozsah}
Pomocí PCG mohou herní světy nabírat obrovských rozměrů. Lze generovat zdánlivě nekonečné světy, jako například celé galaxie. Hry jako \textit{No Man's Sky} -- které se věnuje následující část -- je téměř nemožné vytvořit ručně.

\subsubsection*{Znovuhratelnost}
Unikátní obsah vytvořený pomocí PCG může zajistit, že hra bude pro hráče při každém průchodu jiná a~tím zůstane zábavná i~při několikátém hraní. PCG může v~tomto případě přimět hráče se ke stejné hře opakovaně vracet.

\section{Přístupy k PCG}
Existuje vícero možností, jak k~PCG přistupovat. Jednotlivé možné přístupy jsou popsány v~knize \textit{Procedural Content Generation in Games} \cite{shaker2016procedural}. V~této části jsou popsány jak přístupy, které jsou aplikovatelné při řešení problému procedurálního generování budov, tak přístupy hodící se při generování jiného obsahu.

\subsection{Přístup založený na vyhledávání}
Přístup založený na vyhledávání spočívá v~hodnocení každého vygenerovaného artefaktu. Artefakt je přijat pouze pokud splní minimální požadované hodnocení, jinak je generován znovu. Artefakty, které hodnocení splňují, je následně možno kombinovat nebo ještě vylepšovat.

Základní komponenty jsou pro tento přístup následující:
\begin{itemize}
	\item \textit{Vyhledávací algoritmus}. Jedná se o~hlavní komponentu tohoto přístupu. Relativně jednoduché evoluční algoritmy jsou často zcela dostačující, ovšem mohou nastat situace kdy je výhodnější využít komplexnější algoritmus.
	\item \textit{Reprezentace obsahu}. Jedná se o~reprezentaci artefaktů, které chceme generovat. Reprezentace obsahu definuje (a~tím i~limituje), jaký obsah může být vygenerován, a~určuje, zda je možné efektivní vyhledávání.
	\item \textit{Vyhodnocovací funkce}. Tato funkce nám pro každý artefakt vrátí číselné ohodnocení daného artefaktu. Výsledek tohoto ohodnocení nám může udávat například hratelnost vygenerované úrovně. Vytvoření vyhodnocovací funkce, která spolehlivě ohodnotí aspekty daného artefaktu, patří mezi nejsložitější úkoly při implementaci přístupu založeném na vyhledávání.
\end{itemize}

\subsection{Metody konstruktivního generování}
Metody konstruktivního generování na rozdíl od přístupu založeném na vyhledávání neohodnocují své výstupy, za účelem je následně přegenerovat. Většina těchto metod je zároveň jednoduchá na implementaci. Nejčastější použití mají tyto metody při generování \textit{dungeonů}, ovšem dají se použít i~pro generování jiného videoherního obsahu. Pojem \textit{dungeon} definuje \textit{N. Shaker}~\cite{shaker2016procedural} následovně:

\begin{definition}[Dungeon]
Jde o~labyrintové prostředí skládající se z~navazujících výzev, odměn a~hádanek poskládaných smysluplně v~čase a~prostoru tak, aby vytvářely vysoce strukturovaný videoherní zážitek.
\end{definition}

Dungeony se nemusí ovšem nacházet pouze v~počítačových hrách -- příkladem může být klasická papírová (nepočítačová) hra na hrdiny s~názvem \textit{Dračí doupě} (nebo její původní zahraniční verze \textit{Dungeons \& Dragons}). Ve většině počítačových RPG her se \textit{dungeony} skládají z~několika místností, které jsou propojeny chodbami. Může se jednat o~jeskynní komplexy, lidské stavby a~další. Procedurálním generováním \textit{dungeonů} se rozumí generování topologie, geometrie a~objektů jinak souvisejících s~hraním úrovně tohoto typu. Typicky se metoda generující \textit{dungeony} skládá z~následujících tří prvků.

\begin{enumerate}
	\item \textit{Reprezentativní model}. Abstraktní zjednodušený model reprezentující samotný \textit{dungeon}.
	\item \textit{Metoda ke zkonstruování reprezentativního modelu.}
	\item \textit{Metoda vytvářející reálnou geometrii dungeonu z~jeho reprezentativního modelu.}
\end{enumerate}

Metod konstruktivního generování je mnoho. Následující části textu se věnují dvěma metodám, které jsou nejvíce používány při procedurálním generování dungeonů. První metodou je \textit{metoda dělení prostoru} a~druhou \textit{metoda agentem řízeného růstu}.

\subsubsection*{Metoda dělení prostoru} \label{chapter:bsp}
U~metody dělení prostoru nám algoritmus rozděluje zadaný prostor na disjunktní podmnožiny tak, aby každá část zadaného prostoru ležela v~jedné z~těchto podmnožin. Tyto algoritmy často pracují hierarchicky -- každá podmnožina zadaného prostoru je algoritmem rekurzivně dále dělena. To umožňuje jednotlivé podmnožiny seřadit v takzvaném \textit{prostor-dělícím stromu}.

Nejpopulárnější metodou pro dělení prostoru je \textit{binární dělení prostoru} (binary space partitioning -- BSP), která rekurzivně dělí prostor na dvě podmnožiny. U~této metody můžeme prostor reprezentovat pomocí binárního stromu, který nazýváme \textit{BSP strom}. V~popisu algoritmu \ref{alg:BSPAlgo}~je popsán konkrétní algoritmus používající BSP strom k~dělení prostoru za účelem vytvoření dungeonu. Tento algoritmus byl převzat z~již zmíněné knihy~\cite{shaker2016procedural}. Grafická reprezentace algoritmu je znázorněna na obrázku \ref{fig:BSPSpaceDividing}.

\makeatletter
\renewcommand*{\ALG@name}{Algoritmus}
\makeatother

\begin{algorithm}
\caption{Algoritmus dělení prostoru s využitím BSP stromu}
\begin{algorithmic}[1]
\State Začni s celým prostorem dungeonu (kořen BSP stromu)
\State Rozděl zvolenou oblast podél vertikální nebo horizontální osy na dvě nové oblasti
\State Vyber jednu ze dvou nových oblastí
\If{Oblast je větší, než minimální akceptovatelná velikost}
    \State Jdi na krok 2 (Použij tuto oblast jako oblast určenou k dělení)
\EndIf
\State Vyber druhou oblast a jdi na krok 4
\For {Každou oblast}
	\State Vytvoř místnost v této oblasti tak, že náhodně vybereš dva body (levý horní a pravý spodní) uvnitř zvolené oblasti
\EndFor
\State Počínaje nejnižší úrovní BSP stromu, vytvoř chodby spojující jednotlivé místnosti, které jsou potomky stejného rodiče v BSP stromu
\State Opakuj krok 11 dokud nejsou spojeni potomci kořene BSP stromu
\end{algorithmic}
\label{alg:BSPAlgo}
\end{algorithm}

\begin{figure}
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{research/BSP1}
  \caption{}
  \label{fig:BSP1}
\end{subfigure}%
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{research/BSP2}
  \caption{}
  \label{fig:BSP2}
\end{subfigure}
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{research/BSP3}
  \caption{}
  \label{fig:BSP3}
\end{subfigure}%
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{research/BSP4}
  \caption{}
  \label{fig:BSP4}
\end{subfigure}
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{research/BSP5}
  \caption{}
  \label{fig:BSP5}
\end{subfigure}%
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{research/BSP6}
  \caption{}
  \label{fig:BSP6}
\end{subfigure}
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{research/BSP7}
  \caption{}
  \label{fig:BSP7}
\end{subfigure}%
\caption[Dělení prostoru s~využitím BSP stromu]{Grafická reprezentace Algoritmu dělení prostoru pomocí BSP stromu. Prostor je nejprve rozdělen na podmnožiny B~a~C. Podmnožina B~je následně rozdělena na podmnožiny D~a~E. Jak podmnožina D, tak podmnožina E~jsou již moc malé na další dělení, tudíž zůstanou jako listy v BSP stromu. Algoritmus se přesouvá na podmnožinu C~a~pokračuje stejně jako u~předchozí podmnožiny.}
\label{fig:BSPSpaceDividing}
\end{figure}

\subsubsection*{Metoda agentem řízeného růstu}
Další metodou konstruktivního generování je \textit{metoda agentem řízeného růstu}. Zjednodušeně se tato metoda dá popsat jako metoda, při které jeden agent postupně kope tunely a~tím vytváří chodby a~místnosti. Tato metoda se více hodí k vytvoření organického a~chaotického prostoru, jako jsou například jeskyně. Vzhled výsledného dungeonu vytvořeného tímto přístupem se liší podle vlastností, které nastavíme agentovi. Pokud agenta implementujeme jako z~většiny náhodného, může jeho postup vyústit v~chaotickou změť chodeb, které se mohou v~mnoha místech protínat a~vytvářet tak dungeon připomínající spíše bludiště. To ovšem nemusí být na škodu a~možná je tohle právě cíl, kterého chce tvůrce používající tuto metodu dosáhnout. Agenta můžeme zároveň implementovat tak, aby dával větší pozor na to, kam kope, a~tím nevytvářel již zmíněné protínající se chodby. To ovšem záleží na preferencích samotných tvůrců.

\section{PCG v praxi}
Jak už vyplývá z předchozího textu, je mnoho způsobů, jak PCG při vývoji své hry (a~nejen hry) využít. Nejběžněji je tento nástroj využit při generování světů, ale generovat mohou vývojáři například také nepřátele, hudbu či dokonce příběh. V~této části jsou popsané zajímavé způsoby použití PCG při vývoji počítačové hry.

\subsection{Procedurálně generované světy}
Jak je shrnuto v~článku s~titulem \textit{The Daggerfall Paradox}~\cite{brogan_2016}, ač je PCG mocným nástrojem, vygenerované světy často postrádají duši a~jejich jednotlivé části mohou působit na pozorovatele až identicky. To je dáno samozřejmě tím, že tvůrce odevzdává část tvůrčího procesu do rukou počítače. Zmíněný článek tuto zásadní nevýhodu ilustruje na dvou videoherních titulech. Jedním je \textit{The Elder Scrolls II: Daggerfall} a~druhým \textit{No Man's Sky}. Ač mezi vydáním obou her uběhlo zhruba dvacet let, tyto hry mají jeden společný charakterový rys -- u obou vsadili jejich tvůrci na procedurálně generovaný svět.

\textit{Daggerfall} se dnes jako druhý díl legendární série již řadí mezi kultovní počítačové hry. Jedná se o~jeden z~prvních titulů, kde tvůrci nechali počítač vytvořit gigantický procedurálně generovaný svět. Rozloha tohoto světa odpovídala zhruba čtvrtině rozlohy Spojeného království Velké Británie a Severního Irska.

S hrou \textit{No Man's Sky} je to podobné, ač v~mnohem větším měřítku - hráč v~ní může prozkoumat až 18 kvintilionů (18 000 000 000 000 000 000) unikátních planet a~měsíců, které byly vygenerovány pomocí počítačového algoritmu. Procedurálně generovaný obsah je u~této hry klíčovou vlastností -- závisí na něm celkový pocit ze hraní a~celý smysl hry je právě objevování zdánlivě nekonečného procedurálně generovaného vesmíru. Rozhodnutí použít procedurální generování úzce souviselo s~rozhodnutím, o~jaký typ hry se bude jednat.

\textit{No Man's Sky} bylo ve své době velmi očekávaným titulem, ovšem po jejím vydání byla tato hra pro většinu hráčů obrovským zklamáním. Na vině byla právě zdánlivá identičnost jednotlivých planet. Autor zmíněného článku~\cite{brogan_2016} připodobňuje důvody neúspěchu této hry právě k~\textit{Daggerfallu}, jedné z prvních her, kde bylo PCG použito pro generování otevřeného světa a~kde tvůrci narazili na podobné problémy již o~dvacet let dříve.

\textit{Daggerfall} v~době svého vydání (1996) také vystupoval z~řady právě kvůli do té doby nevídané rozloze herního světa (tento svět je dodnes jedním z největších videoherních světů vůbec). Ač byla ve hře spousta možností co dělat a~co objevovat, a~ač činy hráče mohly ovlivnit osud království, svět této hry rozhodně nevzbuzoval dojem, že by byl vytvořen hráči na míru. Aby vývojáři docílili onoho dojmu realistické rozlohy světa, uchýlili se k~podobné, ač mnohem primitivnější strategii, jako vývojáři \textit{No Man's Sky}. K PCG se vývojáři podle článku~\cite{brogan_2016} uchýlili z~toho důvodu, aby v hráči vzbudili pocit, že opravdu žije v obrovském fantasy světě. Zároveň bylo jejich cílem nenutit hráče vydat se jedinou předepsanou cestou. Výsledkem nakonec bohužel bylo, že všechna místa, kam hráč přišel, vypadala relativně podobně. Města měla málo charakteristických rysů, podle kterých by šla od sebe oddělit a~dungeony byly zřídkakdy zapamatovatelné. A~tento zásadní nedostatek bohužel podědil i~následovník \textit{Daggerfallu} -- o dvacet let mladší \textit{No Man's Sky}.

\subsection{Sadow of Mordor}
Vývojáři hry \textit{Shadow of Mordor} a~později jejího pokračování \textit{Shadow of War} vsadili na PCG při generování nepřátel. V článku \textit{7 uses of procedural generation that all developers should study} \cite{developer_2016} je popsáno, jak pomocí procedurálního generování vývojáři vytváří vztah mezi hráčem a~jednotlivými nepřáteli -- skřety -- z~nichž každý je unikátní, právě díky PCG.

Ve hře je procedurálně generovaný jak vzhled jednotlivých skřetů, tak jejich jméno, nebo dokonce vztah k ostatním skřetům. Skřeti jsou ve hře povýšeni, pokud se jim podaří zabít hráče. V takovém případě si skřeti hráče pamatují při jejich dalším setkání, což pomáhá právě budování vztahu mezi hráčem a~nepřítelem. Pokud je naopak skřet přemožen hráčem a~v boji zraněn, při příštím setkání k~jeho vzhledu přibude například procedurálně generovaná jizva nebo páska přes oko.

Tento systém, pomocí kterého vývojáři budují vztah mezi hráčem, jednotlivými skřety a~okolním světem nazvali samotní tvůrci \textit{Nemesis System} -- ten je dokonce patentován a~je podrobněji popsán v článku \textit{~10 Things To Know About Shadow Of War's Nemesis System}~\cite{chrysostomou_2021}.

\begin{figure}
	\centering
	\includegraphics[width=1.0\textwidth, keepaspectratio]{research/ShadowOfWarOrc}
	\caption[Příklad procedurálně generovaného skřeta ze hry Shadow of War]{Příklad procedurálně generovaného skřeta ze hry Shadow of War -- obrázek z článku \textit{10 Things To Know About Shadow Of War's Nemesis System} \cite{chrysostomou_2021}}
	\label{fig:ShadowOfWarOrc}
\end{figure}

\subsection{Dungeon Alchemist}
\textit{Dungeon Alchemist} je nástroj, který uživateli umožňuje relativně jednoduše vytvořit libovolný dungeon s~pomocí procedurálního generování. Tento nástroj popsal \textit{J.~R.~Zambrano} ve svém článku \textit{D\&D: Map Your Dungeon With An AI – Dungeon Alchemist} \cite{zambrano_2022}. Tato aplikace uživateli vygeneruje dungeon včetně vybaveného interiéru. S~objekty, kterými je dungeon vybaven, může uživatel následně libovolně manipulovat, popřípadě je měnit. To, jak výsledný dungeon vypadá, je primárně ovlivněno tím, jakou tématiku si uživatel pro jednotlivé místnosti zvolí -- touto tématikou může být například kobka nebo vězení. Dungeon Alchemist ovšem není schopen pracovat bez vstupu od uživatele. Uživatel musí ručně zadat například rozlohu jednotlivých místností nebo již zmíněnou tématiku. Momentální verze zároveň není schopná generovat více pater. Rozdílné je ovšem primárně zaměření tohoto generátoru -- zatímco předchozí zmíněné projekty používaly procedurální generování jako určitou mechaniku, která je součástí počítačové hry, Dungeon Alchemist je zamýšlen primárně jako generátor map k již zmíněné papírové hře Dungeons \& Dragons. Uživatel si tedy vygeneruje dungeon, ten si následně například vytiskne na papír a~s~takto vytvořeným dungeonem je připraven hrát. Příklad vytvořeného dungeonu pomocí aplikace Dungeon Alchemist je k~vidění na obrázku \ref{fig:DungeonAlchemyst}.

\begin{figure}
	\centering
	\includegraphics[width=1.0\textwidth, keepaspectratio]{research/DungeonAlchemist}
	\caption[Příklad vygenerovaného dungeonu pomocí nástroje Dungeon Alchemist]{Příklad vygenerovaného dungeonu (zde je dungeonem budova) pomocí nástroje Dungeon Alchemist. Obrázek převzat přímo z oficiálního webu tohoto nástroje -- \textit{www.dungeonalchemist.com}.}
	\label{fig:DungeonAlchemyst}
\end{figure}



