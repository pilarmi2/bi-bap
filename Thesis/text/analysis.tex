\chapter{Analýza}
Tato kapitola se věnuje analýze jednotlivých metod a~technologií, které je možné použít v praktické části práce -- je zde vysvětleno, proč byly dané technologie zvoleny, a~zároveň jsou popsány jejich výhody a~nevýhody. Kapitola je rozdělena do dvou částí -- nejprve se věnuje technologiím zaměřeným na tvorbu 3D modelů, ať už se jedná o~samotné modelování, či tvorbu materiálů. Ve~druhé části jsou pak následně analyzovány technologie, které je možné použít při implementaci algoritmu procedurálního generování.

\section{Tvorba modelů}
Tvorba modelů je klíčovou součástí praktické části práce. Veškeré modely, včetně materiálů, budou vytvořeny ručně, k čemuž je potřeba vybrat sadu odpovídajících nástrojů a~metod. V~této části jsou popsány nástroje a~metody, které je možné použít při tvorbě modelů a~je zde zdůvodněno, proč byly dané nástroje a~metody zvoleny pro použití v~praktické části práce.

\subsection{Modelování}
Modelování bude provedeno v~nástroji \textit{Blender}. Ač tomu tak dříve nebylo, po verzi \textit{2.8} začal být Blender vnímán v~širší komunitě 3D grafiků jako nástroj schopný konkurovat profesionálním komerčním nástrojům, jakými jsou například \textit{Autodesk 3D Studio MAX}, \textit{Autodesk Maya} nebo \textit{Cinema 4D}.

Ač zmíněné komerční nástroje mohou v~některých oblastech překonávat zvolený software, \textit{Blender} má oproti nim jednu klíčovou výhodu -- jedná se totiž o~svobodný a~otevřený software. Od již zmíněné verze \textit{2.8} navíc výrazně stoupla jeho kvalita (kvalita stoupá již roky, ovšem tato verze je v~očích mnoha 3D grafiků zásadním zlomem), tudíž se jedná o~ideální software pro začínající 3D grafiky. Většina klíčových procesů je navíc v~\textit{Blenderu} podobná zmíněným komerčním softwarům. Člověk ovládající \textit{Blender} by tedy měl být snadno schopen přejít na jiný software, který je v~daném týmu standardem. Blender má také jako svobodný a~otevřený software obrovskou komunitu, která je aktivní na nejrůznějších fórech a~webech, a~která svou aktivitou velmi přispívá k~vývoji samotného softwaru -- často se například stává, že plugin vytvořený členem této komunity je v~budoucích verzích zahrnut do základního softwaru.

Další výhodou zvoleného softwaru je, že bude zároveň cílovým softwarem, pro který bude v~praktické části práce vytvořen plugin, respektive sada pluginů, umožňující procedurální generování budov včetně interiérů. Nebude tedy zbytečně zvýšen počet potřebných nástrojů pro~realizaci praktické části této práce.

Blender bude zároveň použit pro UV unwrapping vytvořených modelů. Tento proces, který je podrobněji popsán v~první kapitole, patří mezi závěrečné kroky při vytváření jakéhokoliv modelu -- tento krok je potřeba provést předtím, než začneme pro daný model vytvářet odpovídající materiály. Ač se UV unwrapping může zdát jako proces související více s~texturováním, a~tím pádem by někdo mohl očekávat, že se bude provádět v~softwaru určenému pro texturování, je standardem, že UV unwrapping provádíme ve stejném softwaru, ve kterém modelujeme. Do texturovacích softwarů se následně importuje model s~již provedeným UV unwrappingem. Blender, stejně jako komerční nástroje, navíc disponuje funkcí, která celý proces provádí automaticky, ovšem ta, ač proces značně urychlí, není dokonalá, a~je mnohdy lepší provést unwrapping ručně.

\subsection{Sculpting}
Jedním z~prvních, ne-li vůbec první software umožňující sculpting, byl \textit{ZBrush}. Od té doby tento software byl, a~dodnes je standardem co se týče sculptingu -- jak je také řečeno v~článku \textit{What is ZBrush: How It Works \&~What It’s Used For} \cite{petty_2018}.

Největší slabina softwaru \textit{ZBrush} může být pro většinu uživatelů jeho uživatelské rozhraní -- začátečníkům může dělat problém se v~komplexním rozhraní orientovat. ZBrush je zároveň komerčním nástrojem a~začínající 3D grafici mohou tedy raději sáhnout po otevřeném softwaru, jakým je již zmíněný Blender.

Blender krom modelování umožňuje i~sculpting. Výhodou je zde již zmíněná otevřenost softwaru. Další výhodou může být, že v~případě, kdy uživatel používá Blender pro modelování, nemusí kvůli sculptingu přecházet na jiný software. ZBrush ovšem zůstává mnohem komplexnějším nástrojem, co se týče sculptingu, a~jak již bylo řečeno, jedná se o průmyslový standard.

Při realizaci praktické části práce bude při sculptingu použit opět software Blender. Ač je ZBrush mnohem komplexnější nástroj, jeho veškeré výhody by nebylo možné při projektu tohoto rozsahu využít. Vzhledem ke~zvolené stylizaci modelů, která je popsána v~následujícím textu a~vzhledem k~tomu, že většina modelů určených ke generování jsou neorganické modely, nebude sculpting při jejich vytváření klíčový. Pokud bude vůbec sculpting použit, bude využit při~modelování pouze k~přidání pocitu větší organičnosti modelu tam, kde se to hodí.

\subsection{Materiály}
Existuje mnoho softwarů a~způsobů, jak vytvořit materiály pro vytvořené 3D modely. Ač dříve byl standardní postup ten, že textury se malovaly přímo na 2D UV mapy modelu, dnes se již texturuje pomocí malování na samotné 3D modely -- to ovšem neznamená, že bychom se mohli vyhnout UV unwrappingu. Momentálně jsou v~tomto průmyslu nejvíce používané dva softwary -- software \textit{Substance Painter} vlastněný společností \textit{Adobe} a~software \textit{Mari} vlastněný společností \textit{Foundry}. Rozdíly mezi těmito dvěma nástroji a~doporučení, kdy se jaký z~nich hodí více, jsou popsány v~článku \textit{Substance Painter VS Mari Which is Better?} \cite{inspirationtuts_2021}.

\textit{Mari} je jasnou volbou pro většinu tvůrců, kteří vytvářejí textury pro vizuální efekty filmů. Hlavním důvodem je to, že Mari je vytvořen za účelem co nejsnadnějšího vytváření materiálů a~textur s velmi vysokým rozlišením. Vysoké rozlišení může být důležité například při scénách, kdy je vytvořený objekt zabírán z~velké blízkosti. Tento software byl vytvořen společností \textit{Weta Digital} aby byl použit při produkci filmů \textit{King Kong} a~\textit{Avatar}.

\textit{Substance Painter} je průmyslovým standardem co se týče vytváření materiálů a~textur do~počítačových her. To je hlavním důvodem, proč byl tento nástroj zvolen při realizaci praktické části této práce. Mezi hlavní výhody tohoto softwaru patří relativní jednoduchost použití a~možnost dosáhnout velmi rychle vysoce kvalitních výsledků. \textit{Substance Painter} byl použit například při tvorbě obou části videohry s názvem \textit{The Last of Us}.

\begin{figure}
	\centering
	\includegraphics[width=1.0\textwidth, keepaspectratio]{analysis/TheLastOfUsPart2}
	\caption[Snímek obrazovky ze hry The Last of Us Part II]{Snímek obrazovky ze hry The Last of Us Part II -- převzat z~webových stránek \textit{www.playstation.com}. Při tvorbě materiálů pro tuto hru byl použit software \textit{Substance Painter}.}
	\label{fig:TheLastOfUsPart2}
\end{figure}

Velkou výhodou \textit{Substance Painteru} je také schopnost vytvářet komplexní materiály relativně snadno. Sám o~sobě má širokou nabídku předpřipravených materiálů, které může uživatel libovolně použít. Zároveň umožňuje použití takzvaných \textit{smart materiálů}, což jsou v~podstatě parametrizovatelné materiály -- uživatel může jednoduše nastavit například poničení omítky u~zdi. Při vytváření vlastních materiálů lze ještě \textit{Substance Painter} rozšířit o~další software s~názvem \textit{Substance Designer}, který umožňuje právě navrhování zcela nových materiálů.

\subsection{Realistické vs. stylizované modely}
To, jak modely ve hře vypadají a~jak celková hra nakonec působí můžeme rozdělit do dvou kategorií -- hry a~jejich světy mohou být buď realistické nebo stylizované. Rozdíl mezi realistickými a~stylizovanými modely, a~mezi technikami použitými pro jeden nebo druhý přístup popsala \textit{Kim Aava} ve svém článku \textit{Realistic vs. Stylized: Technique Overview} \cite{aava_2019}.

U realistických modelů se snaží jejich tvůrci co nejlépe napodobit reálný život -- model nemusí reprezentovat něco, co nutně existuje v~reálném světě, ale musí to vypadat tak, že by to součástí našeho světa být mohlo. Hry, které jsme ovšem brali jako realistické před deseti lety nám dnes již moc realistické nepřipadají. Některé takové hry na nás dnes mohou působit dokonce stylizovaně. To můžeme dát samozřejmě za vinu technologickým limitům v~minulosti. Autorka článku tento fakt demonstruje na videoherní sérii \textit{Uncharted} (ukázka na obrázku \ref{fig:Uncharted}). Tvář hlavního hrdiny této série v~prvním díle postrádala spoustu detailů -- tyto detaily si již dnes mohou tvůrci díky postupujícím technologiím dovolit a~tak vidíme u nejnovějšího dílu velké zlepšení. Ač byl v~roce 2007 první díl \textit{Uncharted} brán jako realisticky vypadající hra, dnes tak působit nemusí.

\begin{figure}
	\centering
	\includegraphics[width=1.0\textwidth, keepaspectratio]{analysis/Uncharted}
	\caption[Posun v realističnosti u videoherní série Uncharted]{Posun v~realističnosti u~videoherní série \textit{Uncharted} -- snímek z~článku \textit{Realistic vs. Stylized: Technique Overview} \cite{aava_2019}.}
	\label{fig:Uncharted}
\end{figure}

Při tvorbě stylizovaných světů nehledíme na to, jak reálně modely vypadají. Tvůrce těchto modelů si může libovolně hrát s~barvami a~tvary, a~docílit tak zajímavých výsledků. Ač dříve bylo u~stylizovaných světů standardem nízké rozlišení modelů, a~u~textur používání pouze barevné složky, dnes narazíme i~na stylizované světy s~vysokým rozlišením, využívající komplikovanější materiály. Ač mohou obě metody působit velmi podobně, díky nulovým restrikcím u~tvorby stylizovaných modelů se výsledky výrazně liší. Stylizované modely mohou mít zpravidla méně detailů nebo například jednodušší tvary oproti modelům realistickým. Tento styl můžeme znát například z~animovaných filmů. Jak mohou stylizované světy vypadat je vidět na obrázku \ref{fig:StylizedExample}. Autorka článku rozděluje stylizované modely do dvou dalších kategorií -- těmito kategoriemi jsou přehnaná stylizace a~minimalistická stylizace. Tyto dvě kategorie jsou podrobněji popsány v~následující části.

\begin{figure}
	\centering
	\includegraphics[width=1.0\textwidth, keepaspectratio]{analysis/StylizedExample}
	\caption[Příklad stylizovaných světů]{Příklad stylizovaných světů -- snímek z~článku \textit{Realistic vs. Stylized: Technique Overview} \cite{aava_2019}. Vlevo jsou příklady minimalistické stylizace, zatímco vpravo jsou ukázky světů vytvořených metodou přehnané stylizace.}
	\label{fig:StylizedExample}
\end{figure}

Při přehnané stylizaci se tvůrce zaměřuje převážně na výraznější detaily a~tvary objektu který modeluje. Drobné detaily, které by bylo nutné do modelu zahrnout, pokud by měl být výsledek realistický, můžeme při tvorbě modelu metodou přehnané stylizace zanedbat. Detaily, které jsou pro objekt esenciální naopak ještě zdůrazníme. Při tvorbě takových modelů je třeba brát v~potaz důležitost daného detailu pro objekt jako celek -- je onen detail potřeba, aby pozorovatel například pochopil o~jaký objekt se jedná? Pokud je odpověď na tuto otázku záporná, detail může tvůrce vynechat.

Minimalistická stylizace je, jak už z názvu vyplývá, založena na jednoduchosti. Pro tuto stylizaci se často používají jednoduché tvary a~barvy - při tvorbě materiálů zpravidla používáme pouze barevnou složku a~vynecháváme například normal mapy. Tvůrce modelu metodou minimalistické stylizace zanedbává většinu detailů a~modely ponechává co nejjednodušší.

V praktické části této práce budou vytvořeny modely metodou minimalistické stylizace. Výhodou této metody je, že nechává svému tvůrci volnou ruku. Zároveň je vytváření modelů touto metodou nejjednodušší -- tvorba realistických modelů by drasticky zvýšila časovou náročnost praktické části této práce. Výsledný plugin bude ovšem fungovat tak, aby mohly být jednotlivé modely jednoduše zaměnitelné, a~tudíž mohl uživatel zvolený styl modelů snadno změnit.

\begin{figure}
	\centering
	\includegraphics[width=1.0\textwidth, keepaspectratio]{analysis/StylizedVsRealistic}
	\caption[Porovnání realistického a~stylizovaného modelu]{Porovnání realistického a~stylizovaného modelu -- snímek z~článku \textit{Realistic vs. Stylized: Technique Overview} \cite{aava_2019}.}
	\label{fig:StylizedVsRealistic}
\end{figure}

\section{Implementace algoritmu PCG}
V~této části jsou popsány zvolené technologie a~metody, které budou použity při implementaci generátoru v~praktické části této práce. Podrobněji se návrhu samotné implementace věnuje následující kapitola.

\subsection{Použité technologie}
Existuje několik různých způsobů, jak přistoupit k~samotné implementaci algoritmu PCG. V~praktické části práce bude algoritmus implementován jako plugin do softwaru Blender -- jak je popsáno v~zadání. Další možností by například bylo algoritmus implementovat přímo v~rámci jednoho z~videoherních enginů, jako je například Unity. Oba přístupy mají své výhody i~nevýhody.

Vytvoření pluginu do softwaru Blender umožní uživateli ihned po vygenerování snadno manipulovat s~vygenerovaným modelem -- ať už na úrovni objektů nebo například jednotlivých vrcholů. Tento přístup se nejvíce hodí pro procedurální generování za účelem návrhu obsahu. Vygenerované modely je ovšem následně nutné exportovat z~Blenderu a~importovat je do videoherního enginu. Implementace PCG v~rámci videoherního enginu znamená, že se uživatel nemusí zabývat exportem a~importem vytvořených modelů. Nevýhodou zde ovšem je náročnější manipulace s~objekty -- například na úrovni jednotlivých vrcholů -- nebo manipulace s~materiály. Implementace PCG v~rámci herního enginu dává největší smysl, pokud chceme aby se jednalo přímo o~mechaniku dané hry -- tedy aby se nám například generoval svět v~reálném čase, během hry.

Algoritmus bude konkrétně implementován jako zásuvný modul do softwaru Blender verze \textit{3.1.2}. Pluginy se do tohoto softwaru implementují v jazyce \textit{Python} verze \textit{3.9.7}. Důležité je, aby byl výsledný nástroj pro generování budov vytvořen tak, že půjde snadno rozšířit o~další funkcionality a~že bude snadno zapojen do komplexnějších nástrojů generujících celá města.

Další možností, jak procedurálně vytvářet (nejedná se přímo o~generování) budovy v~softwaru Blender může být použití nástroje zvaného \textit{geometry nodes}. Ač tento nástroj nebude při implementaci budoucího generátoru využit, rozhodně se jedná o~zajímavou alternativu, jak vytvářet budovy a~jiné objekty s~určitým prvkem náhodnosti, a~stojí za to ji zmínit. Krásný příklad, čeho je tento nástroj schopen, zveřejnil autor s~přezdívkou \textit{sozap} na webu \textit{www.blenderartists.org} \cite{sozap_2022}, a~jeden z~výsledků je na obrázku \ref{fig:GeometryNodes}. Jak sám autor v~příspěvku zmiňuje, jedná se o přehnané použití \textit{geometry nodes} a~tento projekt dělal primárně za účelem studia tohoto nástroje. \textit{Geometry nodes} se v~praxi používají v~kombinaci s~klasickým modelováním -- typickým příkladem je například náhodné rozprostření kamenů po zemi.

\begin{figure}
	\centering
	\includegraphics[width=1.0\textwidth, keepaspectratio]{analysis/GeometryNodes}
	\caption[Příklad využití \textit{geometry nodes} při tvorbě budov]{Příklad využití \textit{geometry nodes} při tvorbě budov -- snímek z~příspěvku na webové stránce www.blenderartists.org \cite{sozap_2022}.}
	\label{fig:GeometryNodes}
\end{figure}

\subsection{Použitá metoda generování}
K~samotnému generování bude použita jedna z~\textit{metod konstruktivního generování}. Jak vyplývá z~popisu těchto metod v~první kapitole, pro generování budov a~jejich místností, včetně vybavení, se nejvíce hodí \textit{metoda dělení prostoru}. Tato metoda nám umožní vygenerovat uspořádané místnosti s~rozumně a~smysluplně rozloženým nábytkem. \textit{Metoda agentem řízeného růstu} se hodí spíše pro generování organických dungeonů, jakými mohou být nejčastěji jeskyně. Podrobnější popis budoucího algoritmu se nachází v~následující kapitole.
