import bpy, os, sys
from random import uniform, randint
from bpy.props import *
import random
import requests
import json
import time
import copy
import mathutils
from math import radians
from bpy_extras import object_utils
from bpy.utils import register_class
from bpy.utils import unregister_class
from abc import ABC, abstractmethod

dir, file = os.path.split(os.path.realpath(__file__))

bl_info = {
    "name": "Building generation",
    "description": "Procedural generation of a building.",
    "author": "Michal Pilar",
    "version": (1, 0, 0),
    "blender": (3, 1, 2),
    "location": "Tool > Building generation",
    "warning": "",
    "wiki_url": "",
    "category": "Tool"
}

MAX_SIZE = 20
SEGMENT_SIZE = 5
FLOOR_SIZE = 4
GROUND_SIZE = 0.2

dir, file = os.path.split(os.path.realpath(__file__))
objectsPath = dir + '\\models\\'

wallsModels = ["walls\\wall_only_windows.fbx", "walls\\wall_basic.fbx", "walls\\wall_one_window.fbx"]
wallsWithDoorModels = ["walls\\wall_with_door.fbx"]
wallsInside = ["walls\\wall_inside.fbx"]
wallsInsideDoor = ["walls\\wall_inside_door.fbx"]
floors5x5 = ["floors\\floor.fbx"]
stairs = ["stairs\\stairs.fbx"]
pillars = ["pillars\\pillar.fbx"]
beds = ["inside_objects\\bed.fbx"]
closets = ["inside_objects\\closet.fbx"]
tables = ["inside_objects\\table_with_chairs.fbx"]
kitchenTables = ["inside_objects\\kitchen_table.fbx"]
fireplaces = ["inside_objects\\fireplace.fbx"]

childObjects = ["Window", "Door", "Chair"]


def deleteHierarchy(objectName, myList):
    bpy.ops.object.select_all(action='DESELECT')
    object = bpy.data.objects[objectName]
    object.animation_data_clear()
    names = set()
    outputList = []

    def getChildNames(object):
        for child in object.children:
            names.add(child.name)
            if child.children:
                getChildNames(child)

    getChildNames(object)
    names.add(objectName)

    objects = bpy.data.objects
    [(objects[n].select_set(True)) for n in names]

    for child_name in names:
        bpy.data.objects[child_name].animation_data_clear()

    for object in myList:
        if object not in bpy.context.selected_objects:
            outputList.append(object)

    bpy.ops.object.delete()

    return outputList


def setParent(child, parent, context):
    notChild = True

    for name in childObjects:
        if child.name.startswith(name):
            notChild = False

    if notChild:
        bpy.ops.object.select_all(action='DESELECT')

        child.select_set(True)
        parent.select_set(True)
        context.view_layer.objects.active = parent
        bpy.ops.object.parent_set(type='OBJECT')


def joinObjects(objects, context):
    bpy.ops.object.select_all(action='DESELECT')
    outputObject = None

    for object in objects:
        object.select_set(True)

        if outputObject is None:
            notChild = True

            for name in childObjects:
                if object.name.startswith(name):
                    notChild = False

            if notChild:
                outputObject = object
                context.view_layer.objects.active = outputObject

    bpy.ops.object.join()
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.remove_doubles(threshold=0.01)
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.shade_flat()

    return outputObject


def moveObject(object, x, y, z):
    object.location = (x, y, z)


def rotateObject(object, rotation):
    object.rotation_euler.z += radians(rotation)


class Node:
    _left = None
    _right = None

    def __init__(self):
        pass

    def setRight(self, node):
        self._left = node

    def setLeft(self, node):
        self._right = node


class Direction:
    def __init__(self, x, y):
        self.__xDirection = x
        self.__yDirection = y

    def getXDirection(self):
        return self.__xDirection

    def getYDirection(self):
        return self.__yDirection


class Coords:
    def __init__(self, x, y):
        self.__xCoord = x
        self.__yCoord = y

    def getXCoord(self):
        return self.__xCoord

    def getYCoord(self):
        return self.__yCoord

    def setXCoord(self, x):
        self.__xCoord = x

    def setYCoord(self, y):
        self.__yCoord = y

    def sameCoords(self, coords):
        return coords.getXCoord() == self.__xCoord and coords.getYCoord() == self.__yCoord


class Roof:
    def __init__(self):
        pass

    def spawn(self, segments, floor, context):
        floorSegments = []
        roofSegments = []

        for segment in segments:
            for object in self.spawnSegmentFloor(segment, floor, objectsPath + floors5x5[0]): floorSegments.append(
                object)

        floorObject = joinObjects(floorSegments, context)

        roofObject = floorObject

        return roofObject

    def spawnSegmentFloor(self, segment, floor, path):
        xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2)
        yPosition = segment.getRightLowerCoord().getYCoord() + (SEGMENT_SIZE / 2)

        bpy.ops.import_scene.fbx(filepath=path)
        for object in bpy.context.selected_objects:
            moveObject(object, xPosition, yPosition, (FLOOR_SIZE * floor) + 0.1)

        return bpy.context.selected_objects


class Wall:
    __entranceFloor = False

    def __init__(self, origin):
        self.__coords = [copy.deepcopy(origin)]

    def setEntrance(self):
        self.__entranceFloor = True

    def isEntrance(self):
        return self.__entranceFloor

    def addCoords(self, coords):
        self.__coords.append(copy.deepcopy(coords))

    def getLastCoords(self):
        return self.__coords[-1]

    def getFirstCoords(self):
        return self.__coords[0]

    def getCoords(self):
        return self.__coords

    def clearCoords(self):
        self.__coords.clear()

    def rotateCoords(self):
        newWall = Wall(self.getLastCoords())

        for coords in reversed(self.__coords[:-1]):
            newWall.addCoords(coords)

        if self.isEntrance():
            newWall.setEntrance()

        return newWall

    def getCoordsFromInterval(self, firstCoord, secondCoord):
        outputCoords = []

        partOfInterval = False
        for coords in self.__coords:
            if partOfInterval:
                outputCoords.append(coords)
            if coords.sameCoords(firstCoord):
                partOfInterval = True
            if coords.sameCoords(secondCoord):
                partOfInterval = False

        return outputCoords

    def getLength(self):
        if self.getFirstCoords().getXCoord() == self.getLastCoords().getXCoord():
            return abs(self.getLastCoords().getYCoord() - self.getFirstCoords().getYCoord())
        else:
            return abs(self.getLastCoords().getXCoord() - self.getFirstCoords().getXCoord())

    def containsCoords(self, coords):
        for existingCoords in self.__coords:
            if coords.sameCoords(existingCoords):
                return True

        return False

    def getRotation(self):
        if self.getFirstCoords().getYCoord() == self.getLastCoords().getYCoord():
            if self.getFirstCoords().getXCoord() < self.getLastCoords().getXCoord():
                return 0
            else:
                return 180
        else:
            if self.getFirstCoords().getYCoord() < self.getLastCoords().getYCoord():
                return 90
            else:
                return -90

    def sameWall(self, wall):
        return self.getFirstCoords().sameCoords(wall.getFirstCoords()) and self.getLastCoords().sameCoords(wall.getLastCoords())


class Segment:
    def __init__(self, mainCoord):
        self.__mainCoord = mainCoord

    def getLeftLowerCoord(self):
        return self.__mainCoord

    def getLeftUpperCoord(self):
        return Coords(self.__mainCoord.getXCoord(), self.__mainCoord.getYCoord() + SEGMENT_SIZE)

    def getRightLowerCoord(self):
        return Coords(self.__mainCoord.getXCoord() + SEGMENT_SIZE, self.__mainCoord.getYCoord())

    def getRightUpperCoord(self):
        return Coords(self.__mainCoord.getXCoord() + SEGMENT_SIZE, self.__mainCoord.getYCoord() + SEGMENT_SIZE)

    def containsCoords(self, coords):
        if self.getLeftUpperCoord().sameCoords(coords):
            return True
        elif self.getLeftLowerCoord().sameCoords(coords):
            return True
        elif self.getRightUpperCoord().sameCoords(coords):
            return True
        elif self.getRightLowerCoord().sameCoords(coords):
            return True

    def isInside(self, coords):
        return self.getLeftUpperCoord().getXCoord() <= coords.getXCoord() <= self.getRightUpperCoord().getXCoord() \
               and self.getLeftUpperCoord().getYCoord() >= coords.getYCoord() >= self.getLeftLowerCoord().getYCoord()


class Line:
    def __init__(self, firstCoords, secondCoords):
        self._firstCoords = firstCoords
        self._secondCoords = secondCoords

    def getFirstCoords(self):
        return self._firstCoords

    def getSecondCoords(self):
        return self._secondCoords


class Object:
    _xSize = None
    _ySize = None

    def __init__(self, coords, xSize, ySize):
        self._location = coords
        self._xSize = xSize
        self._ySize = ySize

    def rotate(self):
        tempXSize = self._xSize
        self._xSize = self._ySize
        self._ySize = tempXSize

    def getXSize(self):
        return self._xSize

    def getYSize(self):
        return self._ySize

    def getLocation(self):
        return self._location

    def collisionExists(self, object):
        for firstLine in self.getAllLines():
            for secondLine in object.getAllLines():
                if self.linesIntersection(firstLine, secondLine):
                    return True

        if self.getLeftLowerCoords().getXCoord() > object.getLeftLowerCoords().getXCoord() \
                and self.getRightLowerCoords().getXCoord() < object.getRightLowerCoords().getXCoord() \
                and self.getLeftLowerCoords().getYCoord() > object.getLeftLowerCoords().getYCoord() \
                and self.getLeftUpperCoords().getYCoord() < object.getLeftUpperCoords().getYCoord():
            return True

        if object.getLeftLowerCoords().getXCoord() > self.getLeftLowerCoords().getXCoord() \
                and object.getRightLowerCoords().getXCoord() < self.getRightLowerCoords().getXCoord() \
                and object.getLeftLowerCoords().getYCoord() > self.getLeftLowerCoords().getYCoord() \
                and object.getLeftUpperCoords().getYCoord() < self.getLeftUpperCoords().getYCoord():
            return True

        return False

    def getLeftLowerCoords(self):
        return Coords(self._location.getXCoord() - (self._xSize / 2), self._location.getYCoord() - (self._ySize / 2))

    def getLeftUpperCoords(self):
        return Coords(self._location.getXCoord() - (self._xSize / 2), self._location.getYCoord() + (self._ySize / 2))

    def getRightLowerCoords(self):
        return Coords(self._location.getXCoord() + (self._xSize / 2), self._location.getYCoord() - (self._ySize / 2))

    def getRightUpperCoords(self):
        return Coords(self._location.getXCoord() + (self._xSize / 2), self._location.getYCoord() + (self._ySize / 2))

    def getAllLines(self):
        lines = []

        lines.append(Line(self.getLeftLowerCoords(), self.getLeftUpperCoords()))
        lines.append(Line(self.getLeftUpperCoords(), self.getRightUpperCoords()))
        lines.append(Line(self.getRightLowerCoords(), self.getRightUpperCoords()))
        lines.append(Line(self.getLeftLowerCoords(), self.getRightLowerCoords()))

        return lines

    def linesIntersection(self, firstLine, secondLine):
        if firstLine.getFirstCoords().getXCoord() == firstLine.getSecondCoords().getXCoord():
            if secondLine.getFirstCoords().getXCoord() == secondLine.getSecondCoords().getXCoord():
                pass
            else:
                if secondLine.getFirstCoords().getXCoord() <= firstLine.getFirstCoords().getXCoord() \
                        and secondLine.getSecondCoords().getXCoord() >= firstLine.getSecondCoords().getXCoord() \
                        and secondLine.getFirstCoords().getYCoord() >= firstLine.getFirstCoords().getYCoord() \
                        and secondLine.getSecondCoords().getYCoord() <= firstLine.getSecondCoords().getYCoord():

                    return True
        else:
            if secondLine.getFirstCoords().getYCoord() == secondLine.getSecondCoords().getYCoord():
                pass
            else:
                if secondLine.getFirstCoords().getYCoord() <= firstLine.getFirstCoords().getYCoord() \
                        and secondLine.getSecondCoords().getYCoord() >= firstLine.getSecondCoords().getYCoord() \
                        and secondLine.getFirstCoords().getXCoord() >= firstLine.getFirstCoords().getXCoord() \
                        and secondLine.getSecondCoords().getXCoord() <= firstLine.getSecondCoords().getXCoord():

                    return True

        return False


class Room(ABC):
    _objects = []

    def __init__(self):
        pass

    @abstractmethod
    def spawn(self, floor, floorArea, floorObject, context):
        pass

    @abstractmethod
    def getType(self):
        pass

    def addObject(self, object):
        self._objects.append(object)

    def collidesWithObject(self, object):
        for existingObject in self._objects:
            if existingObject.collisionExists(object):
                return True

        return False

    def clear(self):
        self._objects.clear()

    def isOutsideTheSegment(self, object, segment):
        return not segment.isInside(Coords(object.getLocation().getXCoord() + (object.getXSize() / 2),
                                           object.getLocation().getYCoord() + (object.getYSize() / 2))) \
               or not segment.isInside(Coords(object.getLocation().getXCoord() + (object.getXSize() / 2),
                                              object.getLocation().getYCoord() - (object.getYSize() / 2))) \
               or not segment.isInside(Coords(object.getLocation().getXCoord() - (object.getXSize() / 2),
                                              object.getLocation().getYCoord() + (object.getYSize() / 2))) \
               or not segment.isInside(Coords(object.getLocation().getXCoord() - (object.getXSize() / 2),
                                              object.getLocation().getYCoord() - (object.getYSize() / 2)))

    def spawnObject(self, objectPath, rotate, floor, coords, floorObject, context):
        xPosition = coords.getXCoord()
        yPosition = coords.getYCoord()

        bpy.ops.import_scene.fbx(filepath=objectPath)
        for object in bpy.context.selected_objects:
            notChild = True

            for name in childObjects:
                if object.name.startswith(name):
                    notChild = False

            if notChild:
                moveObject(object, xPosition, yPosition, (FLOOR_SIZE * floor) + (GROUND_SIZE / 2))
                setParent(object, floorObject, context)

                rotation = rotate
                while rotation:
                    rotateObject(object, 90)
                    rotation -= 1

    def wallDirection(self, segment, walls):
        directions = []

        for wall in walls:
            if wall.containsCoords(segment.getLeftLowerCoord())\
                    and wall.containsCoords(segment.getRightLowerCoord())\
                    and not wall.isEntrance():
                directions.append('DOWN')

            if wall.containsCoords(segment.getLeftLowerCoord())\
                    and wall.containsCoords(segment.getLeftUpperCoord())\
                    and not wall.isEntrance():
                directions.append('LEFT')

            if wall.containsCoords(segment.getLeftUpperCoord())\
                    and wall.containsCoords(segment.getRightUpperCoord())\
                    and not wall.isEntrance():
                directions.append('UP')

            if wall.containsCoords(segment.getRightUpperCoord())\
                    and wall.containsCoords(segment.getRightLowerCoord())\
                    and not wall.isEntrance():
                directions.append('RIGHT')

        return directions

    def spawnCloset(self, floor, segment, floorObject, wallsDirections, context):
        wallDirection = random.choice(wallsDirections)
        if wallDirection == 'RIGHT':
            rotate = 2
        elif wallDirection == 'LEFT':
            rotate = 0
        elif wallDirection == 'UP':
            rotate = 3
        else:
            rotate = 1

        if rotate == 0 or rotate == 2:
            paddingX = (SEGMENT_SIZE / 2) - 1
            paddingY = (SEGMENT_SIZE / 2) - 0.8
        else:
            paddingX = (SEGMENT_SIZE / 2) - 0.8
            paddingY = (SEGMENT_SIZE / 2) - 1

        if wallDirection == 'LEFT':
            xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2) - paddingX
            yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2)
        elif wallDirection == 'RIGHT':
            xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2) + paddingX
            yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2)
        elif wallDirection == 'UP':
            xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2)
            yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2) + paddingY
        else:
            xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2)
            yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2) - paddingY

        closet = Object(Coords(xPosition, yPosition), 1, 1.5)

        rotation = rotate
        while rotation:
            closet.rotate()
            rotation -= 1

        if self.collidesWithObject(closet) or self.isOutsideTheSegment(closet, segment):
            return False

        self.addObject(closet)
        self.spawnObject(objectsPath + closets[0], rotate, floor, Coords(xPosition, yPosition), floorObject, context)

        return True

    def spawnTable(self, floor, segment, floorObject, context):
        rotate = bool(random.getrandbits(1))

        xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2)
        yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2)


        table = Object(Coords(xPosition, yPosition), 2, 2.5)
        if rotate:
            table.rotate()

        if self.collidesWithObject(table) or self.isOutsideTheSegment(table, segment):
            return False

        self.addObject(table)
        self.spawnObject(objectsPath + tables[0], rotate, floor, Coords(xPosition, yPosition), floorObject, context)

        return True


class Bedroom(Room):
    def __init__(self):
        super().__init__()

    def spawn(self, floor, floorArea, floorObject, context):
        self.clear()
        segments = floorArea.getSegments()

        counter = 0

        bedsCount = random.randint(1, max(1, len(segments) // 2))
        closetsCount = random.randint(0, max(1, len(segments) // 2))

        while True:
            for segment in segments:
                spawnBed = bool(random.getrandbits(1))
                spawnCloset = bool(random.getrandbits(1))
                spawnBathtub = bool(random.getrandbits(1))
                wallsDirections = self.wallDirection(segment, floorArea.getOutsideWalls())

                if spawnBed and bedsCount > 0 and len(wallsDirections) > 0:
                    if self.spawnBed(floor, segment, floorObject, wallsDirections, context):
                        bedsCount -= 1

                if spawnCloset and closetsCount > 0 and len(wallsDirections) > 0:
                    if self.spawnCloset(floor, segment, floorObject, wallsDirections, context):
                        closetsCount -= 1

            counter += 1

            if (bedsCount == 0 and closetsCount == 0) or counter > 50:
                break

    def spawnBed(self, floor, segment, floorObject, wallsDirections, context):
        wallDirection = random.choice(wallsDirections)
        rotate = bool(random.getrandbits(1))
        if rotate:
            paddingX = (SEGMENT_SIZE / 2) - 1.1
            paddingY = (SEGMENT_SIZE / 2) - 1.3
        else:
            paddingX = (SEGMENT_SIZE / 2) - 1.3
            paddingY = (SEGMENT_SIZE / 2) - 1.1

        if wallDirection == 'LEFT':
            xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2) - paddingX
            yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2) + (
                random.uniform(- paddingY, paddingY))
        elif wallDirection == 'RIGHT':
            xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2) + paddingX
            yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2) + (
                random.uniform(- paddingY, paddingY))
        elif wallDirection == 'UP':
            xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2) + (
                random.uniform(- paddingX, paddingX))
            yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2) + paddingY
        else:
            xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2) + (
                random.uniform(- paddingX, paddingX))
            yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2) - paddingY

        bed = Object(Coords(xPosition, yPosition), 2, 1.6)
        if rotate:
            bed.rotate()

        if self.collidesWithObject(bed) or self.isOutsideTheSegment(bed, segment):
            return False

        self.addObject(bed)
        self.spawnObject(objectsPath + beds[0], rotate, floor, Coords(xPosition, yPosition), floorObject, context)

        return True

    def getType(self):
        return 'BEDROOM'


class Hallway(Room):
    def spawn(self, floor, floorArea, floorObject, context):
        self.clear()
        segments = floorArea.getSegments()

        counter = 0

        closetsCount = random.randint(0, max(1, len(segments) // 2))

        while True:
            for segment in segments:
                spawnCloset = bool(random.getrandbits(1))
                wallsDirections = self.wallDirection(segment,
                                                     floorArea.getOutsideWalls())

                if spawnCloset \
                    and closetsCount > 0 \
                    and len(wallsDirections) > 0\
                    and not segment\
                        .getLeftLowerCoord()\
                        .sameCoords(floorArea.getBuilding()
                                             .getStairsSegment()
                                             .getLeftLowerCoord()):
                    if self.spawnCloset(floor,
                                        segment,
                                        floorObject,
                                        wallsDirections,
                                        context):
                        closetsCount -= 1

            counter += 1

            if closetsCount == 0 or counter > 50:
                break

    def getType(self):
        return 'HALLWAY'


class Kitchen(Room):
    def spawn(self, floor, floorArea, floorObject, context):
        self.clear()
        segments = floorArea.getSegments()

        counter = 0

        kitchenTablesCount = 1
        fireplacesCount = 1

        while True:
            for segment in segments:
                spawnKitchenTable = bool(random.getrandbits(1))
                spawnFireplace = bool(random.getrandbits(1))
                wallsDirections = self.wallDirection(segment, floorArea.getOutsideWalls())

                if spawnKitchenTable and kitchenTablesCount > 0:
                    if self.spawnKitchenTable(floor, segment, floorObject, context):
                        kitchenTablesCount -= 1

                if spawnFireplace and fireplacesCount > 0 and len(wallsDirections) > 0:
                    if self.spawnFireplace(floor, segment, floorObject, wallsDirections, context):
                        fireplacesCount -= 1

            counter += 1

            if (kitchenTablesCount == 0 and fireplacesCount == 0) or counter > 50:
                break

    def spawnKitchenTable(self, floor, segment, floorObject, context):
        rotate = bool(random.getrandbits(1))

        xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2)
        yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2)


        table = Object(Coords(xPosition, yPosition), 2, 1.2)
        if rotate:
            table.rotate()

        if self.collidesWithObject(table) or self.isOutsideTheSegment(table, segment):
            return False

        self.addObject(table)
        self.spawnObject(objectsPath + kitchenTables[0], rotate, floor, Coords(xPosition, yPosition), floorObject, context)

        return True

    def spawnFireplace(self, floor, segment, floorObject, wallsDirections, context):
        wallDirection = random.choice(wallsDirections)
        if wallDirection == 'RIGHT':
            rotate = 2
        elif wallDirection == 'LEFT':
            rotate = 0
        elif wallDirection == 'UP':
            rotate = 3
        else:
            rotate = 1

        if rotate == 0 or rotate == 2:
            paddingX = (SEGMENT_SIZE / 2) - 0.8
            paddingY = (SEGMENT_SIZE / 2) - 0.6
        else:
            paddingX = (SEGMENT_SIZE / 2) - 0.6
            paddingY = (SEGMENT_SIZE / 2) - 0.8

        if wallDirection == 'LEFT':
            xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2) - paddingX
            yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2)
        elif wallDirection == 'RIGHT':
            xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2) + paddingX
            yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2)
        elif wallDirection == 'UP':
            xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2)
            yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2) + paddingY
        else:
            xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2)
            yPosition = segment.getLeftLowerCoord().getYCoord() + (SEGMENT_SIZE / 2) - paddingY

        fireplace = Object(Coords(xPosition, yPosition), 1, 1.5)
        rotation = rotate
        while rotation:
            fireplace.rotate()
            rotation -= 1

        if self.collidesWithObject(fireplace) or self.isOutsideTheSegment(fireplace, segment):
            return False

        self.addObject(fireplace)
        self.spawnObject(objectsPath + fireplaces[0], rotate, floor, Coords(xPosition, yPosition), floorObject, context)

        return True

    def getType(self):
        return 'KITCHEN'


class LivingRoom(Room):
    def spawn(self, floor, floorArea, floorObject, context):
        self.clear()
        segments = floorArea.getSegments()

        counter = 0

        closetsCount = random.randint(0, max(0, len(segments) // 2))
        tablesCount = 1

        while True:
            for segment in segments:
                spawnTable = bool(random.getrandbits(1))
                spawnCloset = bool(random.getrandbits(1))
                wallsDirections = self.wallDirection(segment, floorArea.getOutsideWalls())

                if spawnTable and tablesCount > 0 and not segment.getLeftLowerCoord().sameCoords(floorArea.getBuilding().getStairsSegment().getLeftLowerCoord()):
                    if self.spawnTable(floor, segment, floorObject, context):
                        tablesCount -= 1

                if spawnCloset and closetsCount > 0 and len(wallsDirections) > 0\
                        and not segment.getLeftLowerCoord().sameCoords(floorArea.getBuilding().getStairsSegment().getLeftLowerCoord()):
                    if self.spawnCloset(floor, segment, floorObject, wallsDirections, context):
                        closetsCount -= 1

            counter += 1

            if (closetsCount == 0 and tablesCount == 0) or counter > 50:
                break

    def getType(self):
        return 'LIVINGROOM'


class Building(ABC):
    _floors = []
    _roof = Roof()
    _freeRooms = []
    _mandatoryRooms = []
    _stairsSegment = None

    def __init__(self):
        self._floorPlan = FloorPlan()

    def addFloor(self, floor):
        self._floors.append(floor)

    def getStairsSegment(self):
        return self._stairsSegment

    @abstractmethod
    def prepareRoom(self, segments):
        pass

    def getRoom(self):
        if len(self._mandatoryRooms[-1]) > 0:
            room = self._mandatoryRooms[-1].pop()
        else:
            room = self._freeRooms[-1].pop()
            if room.getType() != 'KITCHEN':
                self._freeRooms[-1].append(room)
        return room

    def spawn(self, context):
        counter = 0
        previousFloorObject = None
        for floor in self._floors:
            floorObject = floor.spawnOutsideWalls(counter, context)
            floorGroundsObject = floor.spawnFloor(counter, context)
            floor.spawnInside(counter, floorObject, context)
            setParent(floorGroundsObject, floorObject, context)

            if counter > 0:
                setParent(floorObject, previousFloorObject, context)
            previousFloorObject = floorObject

            counter += 1

        if context.scene.building.generateRoof:
            setParent(self._roof.spawn(self._floors[0].getSegments(), counter, context), previousFloorObject, context)

    def generate(self, context):
        self.clear()
        while True:
            self.generateFloorPlan()
            if self.validateFloorPlan(context):
                break

        for floor in range(context.floors):
            self.createFloor(context)

    def validateFloorPlan(self, context):
        validationArea = FloorArea(self._floorPlan.getWalls(), self._floorPlan, self, False)
        segments = validationArea.getSegments()
        walls = validationArea.getOutsideWalls()

        if len(segments) < int(context.buildingSize) or len(segments) >= (int(context.buildingSize) + 4):
            return False
        else:
            self._stairsSegment = random.choice(segments)
            random.choice(walls).setEntrance()
            return True

    @abstractmethod
    def createFloor(self, context):
        pass

    def createNewDirection(self, direction):
        randomDirection = random.randint(0, 2)
        if randomDirection == 0:
            return direction
        elif randomDirection == 1:
            return Direction(direction.getYDirection(), direction.getXDirection())
        elif randomDirection == 2:
            return Direction(-direction.getYDirection(), -direction.getXDirection())

    def createWall(self, length, direction, startingCoords):
        wall = Wall(startingCoords)
        currentCoords = copy.deepcopy(startingCoords)

        for i in range(length // 5):
            currentCoords = self.moveCoords(currentCoords, direction)
            wall.addCoords(currentCoords)

        return wall

    def moveCoords(self, coords, direction):
        outputCoords = Coords(coords.getXCoord() + 5 * direction.getXDirection(),
                              coords.getYCoord() + 5 * direction.getYDirection())

        return outputCoords

    def generateFloorPlan(self):
        self._floorPlan.clear()
        originalDirection = Direction(1, 0)
        origin = Coords(0, 0)
        direction = copy.deepcopy(originalDirection)

        while True:
            wallLength = SEGMENT_SIZE
            wall = self.createWall(wallLength, direction, origin)

            if self._floorPlan.validateWall(wall, wallLength, direction):
                self._floorPlan.addWall(wall)
                origin = wall.getLastCoords()
                originalDirection = copy.deepcopy(direction)

            direction = self.createNewDirection(originalDirection)

            if origin.getXCoord() == 0 and origin.getYCoord() == 0:
                break

    def clear(self):
        for floor in self._floors:
            floor.clear()

        for mandatoryRooms in self._mandatoryRooms:
            mandatoryRooms.clear()

        self._mandatoryRooms.clear()
        self._floors.clear()
        self._floorPlan.clear()


class Residential(Building):
    def __init__(self):
        super().__init__()

    def createFloor(self, context):
        if len(self._floors) == 0:
            if context.floors > 1:
                self._mandatoryRooms.append([LivingRoom()])
            else:
                self._mandatoryRooms.append([LivingRoom(), Bedroom(), Kitchen()])
            self._freeRooms.append([Bedroom(), Hallway(), LivingRoom()])
        else:
            self._mandatoryRooms.append([Bedroom()])
            self._freeRooms.append([Kitchen(), Bedroom(), Hallway(), LivingRoom()])

        self.addFloor(FloorArea(self._floorPlan.getWalls(), self._floorPlan, self, True))

    def prepareRoom(self, segments):
        if len(self._mandatoryRooms[-1]) > 0:
            random.shuffle(self._mandatoryRooms[-1])
            room = self._mandatoryRooms[-1][-1]
            for segment in segments:
                if segment.getLeftLowerCoord().sameCoords(self._stairsSegment.getLeftLowerCoord()) \
                        and (room.getType() != 'HALLWAY' and room.getType() != 'LIVINGROOM'):
                    room = Hallway()
                    self._mandatoryRooms[-1].append(room)
        else:
            random.shuffle(self._freeRooms[-1])
            room = self._freeRooms[-1][-1]
            for segment in segments:
                if segment.getLeftLowerCoord().sameCoords(self._stairsSegment.getLeftLowerCoord()) \
                        and (room.getType() != 'HALLWAY' and room.getType() != 'LIVINGROOM'):
                    room = Hallway()
                    self._freeRooms[-1].append(room)

        return room.getType()


class FloorArea(Node):
    def __init__(self, wallsOutside, floorPlan, building, generatingRoom):
        self.__wallsOutside = []
        self.__wallsInside = []
        self.__segments = []
        super().__init__()
        self.__floorPlan = floorPlan
        self.__building = building
        self.__room = None

        for wall in wallsOutside:
            self.__wallsOutside.append(wall)

        self.parseIntoSegments()

        if not generatingRoom:
            return

        maxSegments = 3
        roomType = building.prepareRoom(self.__segments)

        if roomType == 'BEDROOM' or roomType == 'KITCHEN':
            maxSegments = 2
        if roomType == 'LIVINGROOM':
            maxSegments = 4

        if len(self.__segments) > maxSegments:
            while True:
                firstCoords = self.pickRandomCoords()
                secondCoords = self.pickRandomCoords()

                if self.validateNewWallCoords(firstCoords, secondCoords):
                    self.addInsideWall(firstCoords, secondCoords)
                    random.choice(self.__wallsInside).setEntrance()
                    break

            leftWallsOutside, rightWallsOutside = self.splitWalls(firstCoords, secondCoords)

            self._left = FloorArea(leftWallsOutside, floorPlan, building, True)
            self._right = FloorArea(rightWallsOutside, floorPlan, building, True)
        else:
            self.__room = building.getRoom()

    def validateNewWallCoords(self, firstCoords, secondCoords):
        if firstCoords.getXCoord() == secondCoords.getXCoord() or firstCoords.getYCoord() == secondCoords.getYCoord():
            if firstCoords.getXCoord() == secondCoords.getXCoord() and secondCoords.getYCoord() <= firstCoords.getYCoord():
                return False

            if firstCoords.getYCoord() == secondCoords.getYCoord() and secondCoords.getXCoord() <= firstCoords.getXCoord():
                return False

            if not self.belongsToFloorPlan(firstCoords, secondCoords) \
                    or self.collidesWithExistingWall(firstCoords, secondCoords):
                return False
            else:
                return True
        else:
            return False

    def getSegments(self):
        return self.__segments

    def getBuilding(self):
        return self.__building

    def getOutsideWalls(self):
        return self.__wallsOutside

    def splitWalls(self, firstCoords, secondCoords):
        leftWallsOutside = []
        rightWallsOutside = []

        firstCoordsPassed = False
        secondCoordsPassed = False
        leftSide = True
        for wall in self.__wallsOutside:
            if wall.containsCoords(firstCoords) and not firstCoordsPassed:
                firstCoordsPassed = True
                newFirstWall = Wall(wall.getFirstCoords())

                newFirstCoords = wall.getCoordsFromInterval(wall.getFirstCoords(), firstCoords)
                newSecondCoords = wall.getCoordsFromInterval(firstCoords, wall.getLastCoords())
                newSecondWall = Wall(firstCoords)

                for coords in newFirstCoords:
                    newFirstWall.addCoords(coords)
                for coords in newSecondCoords:
                    newSecondWall.addCoords(coords)

                if leftSide:
                    if len(newFirstCoords) > 0:
                        leftWallsOutside.append(newFirstWall)
                        for wall in self.__wallsInside:
                            leftWallsOutside.append(wall)
                    if len(newSecondCoords) > 0:
                        rightWallsOutside.append(newSecondWall)
                else:
                    if len(newFirstCoords) > 0:
                        rightWallsOutside.append(newFirstWall)
                        for wall in self.__wallsInside:
                            rightWallsOutside.append(wall)
                    if len(newSecondCoords) > 0:
                        leftWallsOutside.append(newSecondWall)

                leftSide = not leftSide
            elif wall.containsCoords(secondCoords) and not secondCoordsPassed:
                secondCoordsPassed = True
                newFirstWall = Wall(wall.getFirstCoords())

                newFirstCoords = wall.getCoordsFromInterval(wall.getFirstCoords(), secondCoords)
                newSecondCoords = wall.getCoordsFromInterval(secondCoords, wall.getLastCoords())
                newSecondWall = Wall(secondCoords)

                for coords in newFirstCoords:
                    newFirstWall.addCoords(coords)
                for coords in newSecondCoords:
                    newSecondWall.addCoords(coords)

                if leftSide:
                    if len(newFirstCoords) > 0:
                        leftWallsOutside.append(newFirstWall)
                        for wall in reversed(self.__wallsInside):
                            leftWallsOutside.append(wall.rotateCoords())
                    if len(newSecondCoords) > 0:
                        rightWallsOutside.append(newSecondWall)
                else:
                    if len(newFirstCoords) > 0:
                        rightWallsOutside.append(newFirstWall)
                        for wall in reversed(self.__wallsInside):
                            rightWallsOutside.append(wall.rotateCoords())
                    if len(newSecondCoords) > 0:
                        leftWallsOutside.append(newSecondWall)

                leftSide = not leftSide
            else:
                if leftSide:
                    leftWallsOutside.append(wall)
                else:
                    rightWallsOutside.append(wall)

        return leftWallsOutside, rightWallsOutside

    def addInsideWall(self, firstCoords, secondCoords):
        originCoords = copy.deepcopy(firstCoords)

        if firstCoords.getXCoord() == secondCoords.getXCoord():
            for step in range(1, ((secondCoords.getYCoord() - firstCoords.getYCoord()) // SEGMENT_SIZE) + 1):
                wall = Wall(originCoords)
                wall.addCoords(Coords(firstCoords.getXCoord(), firstCoords.getYCoord() + step * SEGMENT_SIZE))
                self.__wallsInside.append(copy.deepcopy(wall))
                originCoords = copy.deepcopy(
                    Coords(firstCoords.getXCoord(), firstCoords.getYCoord() + step * SEGMENT_SIZE))
        else:
            for step in range(1, ((secondCoords.getXCoord() - firstCoords.getXCoord()) // SEGMENT_SIZE) + 1):
                wall = Wall(originCoords)
                wall.addCoords(Coords(firstCoords.getXCoord() + step * SEGMENT_SIZE, firstCoords.getYCoord()))
                self.__wallsInside.append(copy.deepcopy(wall))
                originCoords = copy.deepcopy(
                    Coords(firstCoords.getXCoord() + step * SEGMENT_SIZE, firstCoords.getYCoord()))

    def belongsToFloorPlan(self, firstCoords, secondCoords):
        previousCoords = firstCoords

        if firstCoords.getXCoord() == secondCoords.getXCoord():
            for step in range(1, (secondCoords.getYCoord() - firstCoords.getYCoord()) // SEGMENT_SIZE):
                if not self.coordsPartOfSegment(previousCoords,
                                                Coords(firstCoords.getXCoord(),
                                                       firstCoords.getYCoord() + step * SEGMENT_SIZE)):
                    return False
                previousCoords = Coords(firstCoords.getXCoord(), firstCoords.getYCoord() + step * SEGMENT_SIZE)
        else:
            for step in range(1, (secondCoords.getXCoord() - firstCoords.getXCoord()) // SEGMENT_SIZE):
                if not self.coordsPartOfSegment(previousCoords,
                                                Coords(firstCoords.getXCoord() + step * SEGMENT_SIZE,
                                                       firstCoords.getYCoord())):
                    return False
                previousCoords = Coords(firstCoords.getXCoord() + step * SEGMENT_SIZE, firstCoords.getYCoord())

        return True

    def collidesWithExistingWall(self, firstCoords, secondCoords):
        if firstCoords.getXCoord() == secondCoords.getXCoord():
            for step in range(1, ((secondCoords.getYCoord() - firstCoords.getYCoord()) // SEGMENT_SIZE)):
                if self.coordsPartOfWall(
                        Coords(firstCoords.getXCoord(), firstCoords.getYCoord() + step * SEGMENT_SIZE)):
                    return True
        else:
            for step in range(1, ((secondCoords.getXCoord() - firstCoords.getXCoord()) // SEGMENT_SIZE)):
                if self.coordsPartOfWall(
                        Coords(firstCoords.getXCoord() + step * SEGMENT_SIZE, firstCoords.getYCoord())):
                    return True

        if self.alreadyExistingWall(firstCoords, secondCoords):
            return True

        return False

    def coordsPartOfWall(self, coords):
        for wall in self.__wallsOutside:
            if wall.containsCoords(coords):
                return True
        return False

    def alreadyExistingWall(self, firstCoords, secondCoords):
        for wall in self.__wallsOutside:
            if wall.containsCoords(firstCoords) and wall.containsCoords(secondCoords):
                return True
        return False

    def coordsPartOfSegment(self, firstCoords, secondCoords):
        for segment in self.__segments:
            if segment.containsCoords(firstCoords) and segment.containsCoords(secondCoords):
                return True

        return False

    def parseIntoSegments(self):
        rectangularNet = self.buildNet()

        for row in rectangularNet:
            currentState = 'UNKNOWN'
            for cell in row:
                if currentState == 'UNKNOWN':
                    if self.findWall(cell.getLeftLowerCoord(), cell.getLeftUpperCoord()):
                        self.__segments.append(cell)
                        if self.findWall(cell.getRightLowerCoord(), cell.getRightUpperCoord()):
                            currentState = 'OUTSIDE_FLOOR_PLAN'
                        else:
                            currentState = 'INSIDE_FLOOR_PLAN'
                    else:
                        if self.findWall(cell.getRightLowerCoord(), cell.getRightUpperCoord()):
                            currentState = 'INSIDE_FLOOR_PLAN'
                        else:
                            currentState = 'OUTSIDE_FLOOR_PLAN'
                elif currentState == 'INSIDE_FLOOR_PLAN':
                    self.__segments.append(cell)
                    if self.findWall(cell.getRightLowerCoord(), cell.getRightUpperCoord()):
                        currentState = 'OUTSIDE_FLOOR_PLAN'
                    else:
                        currentState = 'INSIDE_FLOOR_PLAN'
                elif currentState == 'OUTSIDE_FLOOR_PLAN':
                    if self.findWall(cell.getRightLowerCoord(), cell.getRightUpperCoord()):
                        currentState = 'INSIDE_FLOOR_PLAN'
                    else:
                        currentState = 'OUTSIDE_FLOOR_PLAN'

    def buildNet(self):
        rectangularNet = []

        for row in range(self.__floorPlan.getHeight() // SEGMENT_SIZE):
            rectangularNetRow = []
            for column in range(self.__floorPlan.getWidth() // SEGMENT_SIZE):
                rectangularNetRow.append(Segment(
                    Coords(self.__floorPlan.getFloorPlanNetLeftLower().getXCoord() + column * SEGMENT_SIZE,
                           self.__floorPlan.getFloorPlanNetLeftLower().getYCoord() + row * SEGMENT_SIZE)))
            rectangularNet.append(rectangularNetRow)

        return rectangularNet

    def findWall(self, firstCoords, secondCoords):
        for wall in self.__wallsOutside:
            if wall.containsCoords(firstCoords) and wall.containsCoords(secondCoords):
                return True

        return False

    def pickRandomCoords(self):
        allCoords = []
        for wall in self.__wallsOutside:
            for coord in wall.getCoords():
                allCoords.append(coord)

        return random.choice(allCoords)

    def spawnInside(self, floor, floorObject, context):
        for wall in self.__wallsInside:
            if wall.isEntrance():
                path = objectsPath + wallsInsideDoor[0]
            else:
                path = objectsPath + wallsInside[0]
            for object in self.spawnWall(wall, floor, path):
                setParent(object, floorObject, context)
                self.openDoorsAndWindows([object])

        if self._left is not None and self._right is not None:
            self._left.spawnInside(floor, floorObject, context)
            self._right.spawnInside(floor, floorObject, context)
        else:
            self.__room.spawn(floor, self, floorObject, context)

    def spawnOutsideWalls(self, floor, context):
        walls = []
        pillars = []


        for wall in self.__wallsOutside:
            if floor == 0 and wall.isEntrance():
                path = objectsPath + wallsWithDoorModels[0]
            else:
                path = objectsPath + random.choice(wallsModels)

            for object in self.spawnWall(wall, floor, path): walls.append(object)
            for object in self.spawnPillar(wall, floor): pillars.append(object)

        self.openDoorsAndWindows(walls)
        floorObject = joinObjects(walls, context)
        for object in pillars: setParent(object, floorObject, context)

        return floorObject

    def openDoorsAndWindows(self, objects):
        for object in objects:
            notChild = True

            for name in childObjects:
                if object.name.startswith(name):
                    notChild = False

            if not notChild:
                open = random.randint(0, 10)
                if open > 8:
                    if 'Left' in object.name:
                        angle = random.randint(-50, -30)
                    else:
                        angle = random.randint(30, 50)
                    rotateObject(object, angle)

    def spawnPillar(self, wall, floor):
        path = objectsPath + pillars[0]

        xPosition = wall.getFirstCoords().getXCoord()
        yPosition = wall.getFirstCoords().getYCoord()

        bpy.ops.import_scene.fbx(filepath=path)
        for object in bpy.context.selected_objects:
            moveObject(object, xPosition, yPosition, FLOOR_SIZE * floor)
            rotateObject(object, wall.getRotation())

        return bpy.context.selected_objects

    def spawnFloor(self, floor, context):
        floorSegments = []

        for segment in self.__segments:
            if floor > 0 and self.__building.getStairsSegment().getLeftLowerCoord().sameCoords(segment.getLeftLowerCoord()):
                for object in self.spawnSegmentFloor(segment, floor - 1, objectsPath + stairs[0]): floorSegments.append(
                    object)
            else:
                for object in self.spawnSegmentFloor(segment, floor, objectsPath + floors5x5[0]): floorSegments.append(
                    object)

        return joinObjects(floorSegments, context)

    def spawnSegmentFloor(self, segment, floor, path):
        xPosition = segment.getLeftUpperCoord().getXCoord() + (SEGMENT_SIZE / 2)
        yPosition = segment.getRightLowerCoord().getYCoord() + (SEGMENT_SIZE / 2)

        bpy.ops.import_scene.fbx(filepath=path)
        for object in bpy.context.selected_objects:
            moveObject(object, xPosition, yPosition, FLOOR_SIZE * floor)

        return bpy.context.selected_objects

    def spawnWall(self, wall, floor, path):
        xPosition = wall.getFirstCoords().getXCoord() + (
                wall.getLastCoords().getXCoord() - wall.getFirstCoords().getXCoord()) / 2
        yPosition = wall.getFirstCoords().getYCoord() + (
                wall.getLastCoords().getYCoord() - wall.getFirstCoords().getYCoord()) / 2

        bpy.ops.import_scene.fbx(filepath=path)
        for object in bpy.context.selected_objects:
            notChild = True

            for name in childObjects:
                if object.name.startswith(name):
                    notChild = False

            if notChild:
                moveObject(object, xPosition, yPosition, FLOOR_SIZE * floor)
                rotateObject(object, wall.getRotation())

        return bpy.context.selected_objects

    def clear(self):
        if self._left is not None:
            self._left.clear()

        if self._right is not None:
            self._right.clear()

        self.__wallsInside.clear()
        self.__wallsOutside.clear()
        self.__segments.clear()


class FloorPlan:
    def getFloorPlanNetLeftLower(self):
        return self.__floorPlanNetLeftLower

    def getFloorPlanNetRightUpper(self):
        return self.__floorPlanNetRightUpper

    def __init__(self):
        self.__closingFloorPlan = False

        self.__walls = []

        self.__floorPlanNetLeftLower = Coords(0, 0)
        self.__floorPlanNetRightUpper = Coords(0, 0)

    def getXMax(self):
        return self.__floorPlanNetRightUpper.getXCoord()

    def getXMin(self):
        return self.__floorPlanNetLeftLower.getXCoord()

    def getYMax(self):
        return self.__floorPlanNetRightUpper.getYCoord()

    def getYMin(self):
        return self.__floorPlanNetLeftLower.getYCoord()

    def getWalls(self):
        return self.__walls

    def addWall(self, wall):
        self.__walls.append(copy.deepcopy(wall))

        # Changing X coord of the plan net
        if wall.getFirstCoords().getXCoord() > self.__floorPlanNetRightUpper.getXCoord():
            self.__floorPlanNetRightUpper.setXCoord(wall.getFirstCoords().getXCoord())

        if wall.getLastCoords().getXCoord() > self.__floorPlanNetRightUpper.getXCoord():
            self.__floorPlanNetRightUpper.setXCoord(wall.getLastCoords().getXCoord())

        if wall.getFirstCoords().getXCoord() < self.__floorPlanNetLeftLower.getXCoord():
            self.__floorPlanNetLeftLower.setXCoord(wall.getFirstCoords().getXCoord())

        if wall.getLastCoords().getXCoord() < self.__floorPlanNetLeftLower.getXCoord():
            self.__floorPlanNetLeftLower.setXCoord(wall.getLastCoords().getXCoord())

        # Changing Y coord of the plan net
        if wall.getFirstCoords().getYCoord() > self.__floorPlanNetRightUpper.getYCoord():
            self.__floorPlanNetRightUpper.setYCoord(wall.getFirstCoords().getXCoord())

        if wall.getLastCoords().getYCoord() > self.__floorPlanNetRightUpper.getYCoord():
            self.__floorPlanNetRightUpper.setYCoord(wall.getLastCoords().getYCoord())

        if wall.getFirstCoords().getYCoord() < self.__floorPlanNetLeftLower.getYCoord():
            self.__floorPlanNetLeftLower.setYCoord(wall.getFirstCoords().getXCoord())

        if wall.getLastCoords().getYCoord() < self.__floorPlanNetLeftLower.getYCoord():
            self.__floorPlanNetLeftLower.setYCoord(wall.getLastCoords().getYCoord())

    def clear(self):
        for wall in self.__walls:
            wall.clearCoords()

        self.__walls.clear()
        self.__floorPlanNetLeftLower = Coords(0, 0)
        self.__floorPlanNetRightUpper = Coords(0, 0)
        self.__closingFloorPlan = False

    def getWidth(self):
        return self.__floorPlanNetRightUpper.getXCoord() - self.__floorPlanNetLeftLower.getXCoord()

    def getHeight(self):
        return self.__floorPlanNetRightUpper.getYCoord() - self.__floorPlanNetLeftLower.getYCoord()

    def validateWall(self, wall, length, direction):
        if direction.getXDirection() == -1:
            self.__closingFloorPlan = True

        if wall.containsCoords(Coords(0, 0)) \
                and (wall.getLastCoords().getXCoord() != 0 or wall.getLastCoords().getYCoord() != 0) \
                and (wall.getFirstCoords().getXCoord() != 0 or wall.getFirstCoords().getYCoord() != 0):
            return False

        if direction.getYDirection() == 1 and wall.getFirstCoords().getXCoord() == 0:
            return False

        if self.__closingFloorPlan and (direction.getXDirection() == 1):
            return False

        if not self.__closingFloorPlan and direction.getYDirection() == -1:
            return False

        if wall.getFirstCoords().getXCoord() > self.getXMax():
            if (self.getWidth() + (
                    wall.getFirstCoords().getXCoord() - self.__floorPlanNetRightUpper.getXCoord())) > MAX_SIZE:
                return False

        if wall.getLastCoords().getXCoord() > self.getXMax():
            if (self.getWidth() + (
                    wall.getLastCoords().getXCoord() - self.__floorPlanNetRightUpper.getXCoord())) > MAX_SIZE:
                return False

        if wall.getFirstCoords().getXCoord() < self.getXMin():
            if (self.getWidth() + (
                    self.__floorPlanNetLeftLower.getXCoord() - wall.getFirstCoords().getXCoord())) > MAX_SIZE:
                return False

        if wall.getLastCoords().getXCoord() < self.getXMin():
            if (self.getWidth() + (
                    self.__floorPlanNetLeftLower.getXCoord() - wall.getLastCoords().getXCoord())) > MAX_SIZE:
                return False

        if wall.getFirstCoords().getYCoord() > self.getYMax():
            if (self.getHeight() + (
                    wall.getFirstCoords().getYCoord() - self.__floorPlanNetRightUpper.getYCoord())) > MAX_SIZE:
                return False

        if wall.getLastCoords().getYCoord() > self.getYMax():
            if (self.getHeight() + (
                    wall.getLastCoords().getYCoord() - self.__floorPlanNetRightUpper.getYCoord())) > MAX_SIZE:
                return False

        if wall.getFirstCoords().getYCoord() < self.getYMin():
            if (self.getHeight() + (
                    self.__floorPlanNetLeftLower.getYCoord() - wall.getFirstCoords().getYCoord())) > MAX_SIZE:
                return False

        if wall.getLastCoords().getYCoord() < self.getYMin():
            if (self.getHeight() + (
                    self.__floorPlanNetLeftLower.getYCoord() - wall.getLastCoords().getYCoord())) > MAX_SIZE:
                return False

        if self.getWidth() == MAX_SIZE \
                and wall.getFirstCoords().getXCoord() == self.getXMax() \
                and wall.getFirstCoords().getYCoord() > wall.getLastCoords().getYCoord():
            return False

        if self.getWidth() == MAX_SIZE \
                and wall.getFirstCoords().getXCoord() == self.getXMin() \
                and wall.getFirstCoords().getYCoord() < wall.getLastCoords().getYCoord():
            return False

        if self.getHeight() == MAX_SIZE \
                and wall.getFirstCoords().getYCoord() == self.getYMax() \
                and wall.getLastCoords().getXCoord() > wall.getFirstCoords().getXCoord():
            return False

        if self.getHeight() == MAX_SIZE \
                and wall.getFirstCoords().getYCoord() == self.getYMin() \
                and wall.getLastCoords().getXCoord() < wall.getFirstCoords().getXCoord():
            return False

        if wall.getLastCoords().getYCoord() < 0 or wall.getLastCoords().getXCoord() < 0:
            return False

        for existingWall in self.__walls:
            for existingCoords in existingWall.getCoords():
                for newCoords in wall.getCoords()[1:]:
                    if newCoords.getXCoord() == existingCoords.getXCoord() \
                            and newCoords.getYCoord() == existingCoords.getYCoord():
                        if wall.getLastCoords().getXCoord() != 0 \
                                or wall.getLastCoords().getYCoord() != 0:
                            return False

        return True


class MyPlugin:
    def __init__(self):
        self.classes = [MyPanel, ProceduralGeneration, BuildingData]

    def menuFunction(self, context):
        self.layout.operator(ProceduralGeneration().bl_idname)


class MyPanel(bpy.types.Panel):
    bl_idname = "MYPLUGIN_PT_building_generation"
    bl_label = "Building generation"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Tool"
    bl_context = "objectmode"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        building = scene.building

        buildingTypeColumn = layout.column(align=True)
        buildingTypeColumn.prop(building, "buildingType")

        buildingSizeColumn = layout.column(align=True)
        buildingSizeColumn.prop(building, "buildingSize")

        floorsRow = layout.row(align=True)
        floorsRow.prop(building, "floors")

        generateRoofRow = layout.row(align=True)
        generateRoofRow.prop(building, "generateRoof")

        buttonRow = layout.row(align=True)
        buttonRow.operator("object.building")


class BuildingData(bpy.types.PropertyGroup):
    generateRoof: bpy.props.BoolProperty(
        name="Generate roof",
        description="Generate roof",
        default=False
    )


    floors: bpy.props.IntProperty(
        name='Floors',
        description='Number of floors',
        default=1, min=1, max=4, step=1
    )

    buildingTypes = [('RESIDENTIAL', "Residential house", '', '', 0)]

    buildingType: bpy.props.EnumProperty(
        items=buildingTypes,
        name='Building type',
        description='Type of the building',
    )

    buildingSizes = [('4', "Small", '', '', 0),
                     ('8', "Medium", '', '', 1),
                     ('12', "Large", '', '', 2)]

    buildingSize: bpy.props.EnumProperty(
        items=buildingSizes,
        name='Building size',
        description='Type of the building',
    )


class ProceduralGeneration(bpy.types.Operator):
    bl_idname = "object.building"
    bl_label = "Generate building"
    bl_options = {'REGISTER', 'UNDO'}

    def __init__(self):
        self.__building = None

    def execute(self, context):
        if self.__building is not None:
            self.clear()

        self.__building = Residential()

        self.__building.generate(context.scene.building)
        self.__building.spawn(context)

        return {'FINISHED'}

    def clear(self):
        self.__building.clear()


def register():
    for myClass in MyPlugin().classes:
        register_class(myClass)

    bpy.types.Scene.building = PointerProperty(type=BuildingData)


def unregister():
    for myClass in MyPlugin().classes:
        unregister_class(myClass)


if __name__ == "__main__":
    register()
